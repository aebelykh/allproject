package LayerFindWay;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "LayerFindWay")
public class LayerFindWay {
    private String keys;

    public LayerFindWay() {
    }

    public LayerFindWay(String keys) {
        this.keys = keys;
    }


    @XmlElement(name = "Keys")
    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }
}
