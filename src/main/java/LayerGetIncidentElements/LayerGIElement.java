package LayerGetIncidentElements;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "Element")
public class LayerGIElement {
    private int elemID;
    private String keys;

    public LayerGIElement() {
    }

    public LayerGIElement(int elemID, String keys) {
        this.elemID = elemID;
        this.keys = keys;
    }


    @XmlElement(name = "ElemID")
    public int getElemID() {
        return elemID;
    }

    public void setElemID(int elemID) {
        this.elemID = elemID;
    }

    @XmlElement(name = "Keys")
    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }
}
