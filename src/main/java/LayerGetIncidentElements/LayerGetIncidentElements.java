package LayerGetIncidentElements;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlType(name = "LayerGetIncidentElements")
public class LayerGetIncidentElements {
    private List<LayerGIElement> elements = new ArrayList<>();

    public LayerGetIncidentElements() {
    }

    public LayerGetIncidentElements(List<LayerGIElement> elements) {
        this.elements = elements;
    }


    @XmlElementWrapper(name = "Elements")
    public List<LayerGIElement> getElements() {
        return elements;
    }

    @XmlElement(name = "Element")
    public void setElements(List<LayerGIElement> elements) {
        this.elements = elements;
    }
}
