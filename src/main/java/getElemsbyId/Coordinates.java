package getElemsbyId;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlType
public class Coordinates {

    private double Latitude;
    private double Longitude;

    public Coordinates(double latitude, double longitude) {
        this.Latitude = latitude;
        this.Longitude = longitude;
    }

    public Coordinates() {

    }


    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }


    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double longitude) {
        Longitude = longitude;
    }

}