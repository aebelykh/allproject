package getElemsbyId;

import javax.xml.bind.annotation.*;

@XmlType(propOrder = {"elemID","typeID","modeNum","geometry"})
public class Element {

    private int elemID;
    private int typeID;
    private int modeNum;
    private Geometry geometry = new Geometry();

    public Element(int elemID, int typeID, int modeNum, Geometry geometry) {
        this.elemID = elemID;
        this.typeID = typeID;
        this.modeNum = modeNum;
        this.geometry = geometry;
    }

    public Element() {

    }
    @XmlElement(name = "ElemID")
    public int getElemID() {
        return elemID;
    }

    public void setElemID(int elemID) {
        this.elemID = elemID;
    }

    @XmlElement(name = "TypeID")
    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public int getModeNum() {
        return modeNum;
    }

    @XmlElement(name = "ModeNum")
    public void setModeNum(int modeNum) {
        this.modeNum = modeNum;
    }

    @XmlElement(name = "Geometry")
    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

}



