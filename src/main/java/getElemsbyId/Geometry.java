package getElemsbyId;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class Geometry {
    private Placemark placemark = new Placemark();

    public Geometry() {

    }

    private enum Format{KML};
    private Format format = Format.KML;

    public Geometry(Placemark placemark, Format format) {
        this.placemark = placemark;
        this.format = format;
    }
    @XmlElement(name = "Placemark")
    public Placemark getPlacemark() {
        return placemark;
    }

    public void setPlacemark(Placemark placemark) {
        this.placemark = placemark;
    }

    @XmlElement(name = "Format")
    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }
}
