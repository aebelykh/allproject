package getElemsbyId;


import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@XmlType
public class GetElemsByID {
    private List<Element> elements = new ArrayList<>();

    public GetElemsByID() {
    }

    public GetElemsByID(List<Element> elements) {
        this.elements = elements;
    }

    @XmlElement(name = "Element")
    public List<Element> getElements() {
        return elements;
    }

    public void setElements(List<Element> elements) {
        this.elements = elements;
    }
}
