package getElemsbyId;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class Placemark {
    private Point point = new Point();

    public Placemark(Point point) {
        this.point = point;
    }

    public Placemark() {

    }
    @XmlElement(name = "Point")
    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }
}
