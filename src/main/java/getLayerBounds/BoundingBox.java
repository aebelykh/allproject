package getLayerBounds;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
@XmlType(propOrder = {"crs","minx","miny","maxx","maxy"})
public class BoundingBox {
    private String crs;
    private double minx;
    private double miny;
    private double maxx;
    private double maxy;

    public BoundingBox() {
    }

    public BoundingBox(String crs, double minx, double miny, double maxx, double maxy) {
        this.crs = crs;
        this.minx = minx;
        this.miny = miny;
        this.maxx = maxx;
        this.maxy = maxy;
    }

    public String getCrs() {
        return crs;
    }

    @XmlAttribute(name = "CRS")
    public void setCrs(String crs) {
        this.crs = crs;
    }

    public double getMinx() {
        return minx;
    }

    @XmlAttribute(name = "minx")
    public void setMinx(double minx) {
        this.minx = minx;
    }

    public double getMiny() {
        return miny;
    }

    @XmlAttribute(name = "miny")
    public void setMiny(double miny) {
        this.miny = miny;
    }

    public double getMaxx() {
        return maxx;
    }

    @XmlAttribute(name = "maxx")
    public void setMaxx(double maxx) {
        this.maxx = maxx;
    }

    public double getMaxy() {
        return maxy;
    }

    @XmlAttribute(name = "maxy")
    public void setMaxy(double maxy) {
        this.maxy = maxy;
    }
}


