package getLayerBounds;

import zulu.ZwsResponseType;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"bounds"})
public class GetLayerBounds extends ZwsResponseType {
    private ZwsBounds bounds = new ZwsBounds();


    public GetLayerBounds() {
    }

    public GetLayerBounds(ZwsBounds bounds) {
        this.bounds = bounds;

    }

    @XmlElement(name = "Bounds")
    public ZwsBounds getBounds() {
        return bounds;
    }

    public void setBounds(ZwsBounds bounds) {
        this.bounds = bounds;
    }
}
