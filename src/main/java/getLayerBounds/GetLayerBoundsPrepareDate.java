package getLayerBounds;

public class GetLayerBoundsPrepareDate{
    public static void main(String[] args) {

    }

    public static void prepareDate() {
        BoundingBox boundingBox = new BoundingBox();
        boundingBox.setCrs("EPSG:4326");
        boundingBox.setMinx(56.886598);
        boundingBox.setMiny(23.992510);
        boundingBox.setMaxx(56.977003);
        boundingBox.setMaxy(24.116738);

        BoundingBox boundingBox1 = new BoundingBox();
        boundingBox1.setCrs("EPSG:3857");
        boundingBox1.setMinx(7736975.513859);
        boundingBox1.setMiny(2670833.945157);
        boundingBox1.setMaxx(7755419.826169);
        boundingBox1.setMaxy(2684663.016566);


        ZwsBounds zwsBounds = new ZwsBounds();
        zwsBounds.getBoundingBoxes().add(boundingBox);
        zwsBounds.getBoundingBoxes().add(boundingBox1);

    }
}


