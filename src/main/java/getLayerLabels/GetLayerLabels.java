package getLayerLabels;

import zulu.ZwsResponseType;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType
public class GetLayerLabels extends ZwsResponseType {
    private List<ZwsLabelLayers> labelLayers = new ArrayList<>();

    public GetLayerLabels() {
    }

    public GetLayerLabels(List<ZwsLabelLayers> labelLayers) {
        this.labelLayers = labelLayers;
    }

    @XmlElementWrapper(name = "LabelLayers")
    @XmlElement(name = "LabelLayer")
    public List<ZwsLabelLayers> getLabelLayer() {
        return labelLayers;
    }


    public void setLabelLayers(List<ZwsLabelLayers> labelLayers) {
        this.labelLayers = labelLayers;
    }
}
