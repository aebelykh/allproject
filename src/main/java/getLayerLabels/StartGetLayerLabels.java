package getLayerLabels;

public class StartGetLayerLabels {
    public static void main(String[] args) {
    }

    public static GetLayerLabels prepareData() {
        ZwsLabelLayers labelLayer1 = new ZwsLabelLayers();
        labelLayer1.setID(0);
        labelLayer1.setUserName("Siltumavotu parametri");

        ZwsLabelLayers labelLayer2 = new ZwsLabelLayers();
        labelLayer2.setID(1);
        labelLayer2.setUserName("Siltumavoti");

        ZwsLabelLayers labelLayer3 = new ZwsLabelLayers();
        labelLayer3.setID(2);
        labelLayer3.setUserName("Piesleguma datums");

        ZwsLabelLayers labelLayer4 = new ZwsLabelLayers();
        labelLayer4.setID(3);
        labelLayer4.setUserName("Kameru nosaukumi");

        ZwsLabelLayers labelLayer5 = new ZwsLabelLayers();
        labelLayer5.setID(4);
        labelLayer5.setUserName("Aizbidni,koveru nosaukumi");

        ZwsLabelLayers labelLayer6 = new ZwsLabelLayers();
        labelLayer6.setID(5);
        labelLayer6.setUserName("Zad");

        ZwsLabelLayers labelLayer7 = new ZwsLabelLayers();
        labelLayer7.setID(6);
        labelLayer7.setUserName("Abonentu slodzes");

        ZwsLabelLayers labelLayer8 = new ZwsLabelLayers();
        labelLayer8.setID(7);
        labelLayer8.setUserName("Klients");

        ZwsLabelLayers labelLayer9 = new ZwsLabelLayers();
        labelLayer9.setID(8);
        labelLayer9.setUserName("PAC");

        ZwsLabelLayers labelLayer10 = new ZwsLabelLayers();
        labelLayer10.setID(9);
        labelLayer10.setUserName("Abonenta informacija");

        ZwsLabelLayers labelLayer11 = new ZwsLabelLayers();
        labelLayer11.setID(10);
        labelLayer11.setUserName("Kamera_dati");

        ZwsLabelLayers labelLayer12 = new ZwsLabelLayers();
        labelLayer12.setID(11);
        labelLayer12.setUserName("Участки");

        GetLayerLabels getLayerLabels = new GetLayerLabels();
        getLayerLabels.getLabelLayer().add(labelLayer1);
        getLayerLabels.getLabelLayer().add(labelLayer2);
        getLayerLabels.getLabelLayer().add(labelLayer3);
        getLayerLabels.getLabelLayer().add(labelLayer4);
        getLayerLabels.getLabelLayer().add(labelLayer5);
        getLayerLabels.getLabelLayer().add(labelLayer6);
        getLayerLabels.getLabelLayer().add(labelLayer7);
        getLayerLabels.getLabelLayer().add(labelLayer8);
        getLayerLabels.getLabelLayer().add(labelLayer9);
        getLayerLabels.getLabelLayer().add(labelLayer10);
        getLayerLabels.getLabelLayer().add(labelLayer11);
        getLayerLabels.getLabelLayer().add(labelLayer12);

        return getLayerLabels;
    }
}
