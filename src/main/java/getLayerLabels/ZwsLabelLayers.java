package getLayerLabels;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class ZwsLabelLayers {
    private long ID;
    private String UserName;

    public ZwsLabelLayers() {
    }

    public ZwsLabelLayers(long ID, String userName) {
        this.ID = ID;
        UserName = userName;
    }

    @XmlElement(name = "ID")
    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    @XmlElement(name = "UserName")
    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }
}
