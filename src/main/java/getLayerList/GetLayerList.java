package getLayerList;

import zulu.ZwsResponseType;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType(name = "GetLayerList")
public class GetLayerList extends ZwsResponseType {
    private List<ZwsList> List = new ArrayList<>();

    public GetLayerList() {
    }

    public GetLayerList(java.util.List<ZwsList> list) {
        List = list;
    }

    @XmlElement(name = "Layer")
    public java.util.List<ZwsList> getList() {
        return List;
    }

    public void setList(java.util.List<ZwsList> list) {
        List = list;
    }

}
