package getLayerList;

import zulu.ZwsResponse;

public class StartGetLayerList extends ZwsResponse {
    public static void main(String[] args) {

    }

    public static GetLayerList prepareData() {
        ZwsList list1 = new ZwsList();
        list1.setName("riga:houses");
        list1.setTitle("buve 1");

        ZwsList list2 = new ZwsList();
        list2.setName("riga:teplo");
        list2.setTitle("Kreisajs");

        ZwsList list3 = new ZwsList();
        list3.setName("mo:vo");
        list3.setTitle("Водоотведение");

        ZwsList list4 = new ZwsList();
        list4.setName("mo:ts");
        list4.setTitle("Тепловая сеть");

        ZwsList list5 = new ZwsList();
        list5.setName("mo:region");
        list5.setTitle("Районы МО");

        ZwsList list6 = new ZwsList();
        list6.setName("kr:adm");
        list6.setTitle("Административное деление");

        GetLayerList getLayerList = new GetLayerList();
        getLayerList.getList().add(list1);
        getLayerList.getList().add(list2);
        getLayerList.getList().add(list3);
        getLayerList.getList().add(list4);
        getLayerList.getList().add(list5);
        getLayerList.getList().add(list6);

        return getLayerList;
    }
}
