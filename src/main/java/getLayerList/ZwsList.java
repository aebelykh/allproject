package getLayerList;



import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class ZwsList {
    private String Name;
    private String Title;

    public ZwsList() {
    }

    public ZwsList(String name, String title) {
        Name = name;
        Title = title;
    }

    public String getName() {
        return Name;
    }

    @XmlElement(name = "Name")
    public void setName(String name) {
        Name = name;
    }

    public String getTitle() {
        return Title;
    }

    @XmlElement(name = "Title")
    public void setTitle(String title) {
        Title = title;
    }
}
