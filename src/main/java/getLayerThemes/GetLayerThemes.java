package getLayerThemes;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType
public class GetLayerThemes {
    private List<ZwsThemes> themes = new ArrayList<>();

    public GetLayerThemes() {
    }

    public GetLayerThemes(List<ZwsThemes> themes) {
        this.themes = themes;
    }

    @XmlElementWrapper(name = "Themes")
    @XmlElement(name = "Theme")
    public List<ZwsThemes> getTheme() {
        return themes;
    }

    public void setThemes(List<ZwsThemes> themes) {
        this.themes = themes;
    }
}
