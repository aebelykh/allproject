package getLayerThemes;

public class StartGetLayerThemes {
    public static void main(String[] args) {
    }


    public static GetLayerThemes prepareData() {
        ZwsThemes themes1 = new ZwsThemes();
        themes1.setID(1);
        themes1.setUserName("Источники");

        ZwsThemes themes2 = new ZwsThemes();
        themes2.setID(2);
        themes2.setUserName("Участки 0.9");

        GetLayerThemes getLayerThemes = new GetLayerThemes();
        getLayerThemes.getTheme().add(themes1);
        getLayerThemes.getTheme().add(themes2);

        return getLayerThemes;
    }
}
