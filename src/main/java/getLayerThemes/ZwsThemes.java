package getLayerThemes;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class ZwsThemes {
    private long ID;
    private String UserName;

    public ZwsThemes() {
    }

    public ZwsThemes(long ID, String userName) {
        this.ID = ID;
        UserName = userName;
    }

    public long getID() {
        return ID;
    }

    @XmlElement(name = "ID")
    public void setID(long ID) {
        this.ID = ID;
    }

    public String getUserName() {
        return UserName;
    }

    @XmlElement(name = "UserName")
    public void setUserName(String userName) {
        UserName = userName;
    }
}
