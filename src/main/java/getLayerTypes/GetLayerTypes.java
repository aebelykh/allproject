package getLayerTypes;

import zulu.ZwsResponseType;

import javax.naming.Name;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlType
public class GetLayerTypes extends ZwsResponseType {

    private List<ZwsType> types = new ArrayList<>();

    public GetLayerTypes() {
    }


    public GetLayerTypes(List<ZwsType> types) {
        this.types = types;
    }

    @XmlElementWrapper(name = "Types")
    public List<ZwsType> getType() {
        return types;
    }

    @XmlElement(name = "Type")
    public void setType(List<ZwsType> types) {
        this.types = types;
    }
}
