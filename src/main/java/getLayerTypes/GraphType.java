package getLayerTypes;

import com.sun.xml.internal.txw2.annotation.XmlElement;

import javax.xml.bind.annotation.XmlEnum;
@XmlEnum
public enum GraphType {
    POINT,
    LINE
}
