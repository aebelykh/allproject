package getLayerTypes;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "Mode", propOrder = {"index","title","switchState"})
public class Mode {
    private int index;
    private String title;
    private boolean switchState;

    public Mode() {
    }

    public Mode(int index, String title, boolean switchState) {
        this.index = index;
        this.title = title;
        this.switchState = switchState;
    }


    public int getIndex() {
        return index;
    }

    @XmlElement(name = "Index")
    public void setIndex(int index) {
        this.index = index;
    }

    public String getTitle() {
        return title;
    }

    @XmlElement(name = "Title")
    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSwitchState() {
        return switchState;
    }

    @XmlElement(name = "SwitchState")
    public void setSwitchState(boolean switchState) {
        this.switchState = switchState;
    }
}
