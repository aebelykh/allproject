package getLayerTypes;

public class StartGetLayerTypes {
    public static void main(String[] args) {
    }

    public static GetLayerTypes prepareDate(){
        ZwsType type1=new ZwsType();
        type1.setId(-1);
        type1.setTitle("Primitives");

        ZwsType type2=new ZwsType();
        type2.setId(1);
        type2.setTitle("Источник");
        type2.setGraphType(GraphType.POINT);
        type2.setTag(2);

        Mode mode1=new Mode();
        mode1.setIndex(1);
        mode1.setTitle("Работа");
        mode1.setSwitchState(true);

        Mode mode2=new Mode();
        mode2.setIndex(2);
        mode2.setTitle("Отключен");
        mode2.setSwitchState(false);

        ZwsType type3=new ZwsType();
        type3.setId(2);
        type3.setTitle("Узел");
        type3.setGraphType(GraphType.POINT);
        type3.setTag(5);

        Mode mode3=new Mode();
        mode3.setIndex(1);
        mode3.setTitle("Тепловая камера");

        Mode mode4=new Mode();
        mode4.setIndex(2);
        mode4.setTitle("Разветвление");

        Mode mode5=new Mode();
        mode5.setIndex(3);
        mode5.setTitle("Смена диаметра");

        Mode mode6=new Mode();
        mode6.setIndex(4);
        mode6.setTitle("cmena vida prok");

        Mode mode7=new Mode();
        mode7.setIndex(5);
        mode7.setTitle("Kover");

        ZwsType type4=new ZwsType();
        type4.setId(3);
        type4.setTitle("Потpебитель");
        type4.setGraphType(GraphType.POINT);
        type4.setTag(4);

        Mode mode8=new Mode();
        mode8.setIndex(1);
        mode8.setTitle("Работа");

        Mode mode9=new Mode();
        mode9.setIndex(2);
        mode9.setTitle("Отключен");

        ZwsType type5=new ZwsType();
        type5.setId(4);
        type5.setTitle("Насосная станция");
        type5.setGraphType(GraphType.POINT);
        type5.setTag(5);

        Mode mode10=new Mode();
        mode10.setIndex(1);
        mode10.setTitle("Работа");

        ZwsType type6=new ZwsType();
        type6.setId(5);
        type6.setTitle("Задвижка");
        type6.setGraphType(GraphType.POINT);
        type6.setTag(3);

        Mode mode11=new Mode();
        mode11.setIndex(1);
        mode11.setTitle("Открыта");

        Mode mode12=new Mode();
        mode12.setIndex(2);
        mode12.setTitle("Закрыта");

        ZwsType type7=new ZwsType();
        type7.setId(6);
        type7.setTitle("Участки");
        type7.setGraphType(GraphType.LINE);
        type7.setTag(1);

        Mode mode13=new Mode();
        mode13.setIndex(1);
        mode13.setTitle("Включен");
        mode13.setSwitchState(true);

        Mode mode14=new Mode();
        mode14.setIndex(2);
        mode14.setTitle("Отключен");
        mode14.setSwitchState(false);

        Mode mode15=new Mode();
        mode15.setIndex(3);
        mode15.setTitle("Отключ. обратный тp-д");
        mode15.setSwitchState(true);

        Mode mode16=new Mode();
        mode16.setIndex(4);
        mode16.setTitle("Отключ. обратный тp-д");
        mode16.setSwitchState(true);

        ZwsType type8=new ZwsType();
        type8.setId(7);
        type8.setTitle("Дросселирующий узел");
        type8.setGraphType(GraphType.POINT);
        type8.setTag(5);

        Mode mode17=new Mode();
        mode17.setIndex(1);
        mode17.setTitle("Вычиcляемая шайба");

        Mode mode18=new Mode();
        mode18.setIndex(2);
        mode18.setTitle("Устанавливаемая шайба");

        Mode mode19=new Mode();
        mode19.setIndex(3);
        mode19.setTitle("Регулятор напора");

        Mode mode20=new Mode();
        mode20.setIndex(4);
        mode20.setTitle("Регулятор давления в подаюшем");

        Mode mode21=new Mode();
        mode21.setIndex(5);
        mode21.setTitle("Регулятор давления в обратном");

        Mode mode22=new Mode();
        mode22.setIndex(6);
        mode22.setTitle("Регулятор расхода на подающем");

        Mode mode23=new Mode();
        mode23.setIndex(7);
        mode23.setTitle("Регулятор расхода на подающем");

        Mode mode24=new Mode();
        mode24.setIndex(8);
        mode24.setTitle("Регулятор напора на обратном");

        ZwsType type9=new ZwsType();
        type9.setId(8);
        type9.setTitle("ЦТП");
        type9.setGraphType(GraphType.POINT);
        type9.setTag(0);

        Mode mode25=new Mode();
        mode25.setIndex(1);
        mode25.setTitle("ЦТП");

        ZwsType type10=new ZwsType();
        type10.setId(9);
        type10.setTitle("ЦТП");
        type10.setGraphType(GraphType.POINT);
        type10.setTag(0);

        Mode mode26=new Mode();
        mode26.setIndex(1);
        mode26.setTitle("Граница");

        ZwsType type11=new ZwsType();
        type11.setId(10);
        type11.setTitle("Узел учета");
        type11.setGraphType(GraphType.POINT);
        type11.setTag(0);

        Mode mode33=new Mode();
        mode33.setIndex(1);
        mode33.setTitle("Приборы учета");

        ZwsType type12=new ZwsType();
        type12.setId(11);
        type12.setTitle("Перемычка");
        type12.setGraphType(GraphType.POINT);
        type12.setTag(0);

        Mode mode27=new Mode();
        mode27.setIndex(1);
        mode27.setTitle("Включена");

        Mode mode28=new Mode();
        mode28.setIndex(2);
        mode28.setTitle("Закрыта");

        ZwsType type13=new ZwsType();
        type13.setId(12);
        type13.setTitle("Обобщенный потребитель");
        type13.setGraphType(GraphType.POINT);
        type13.setTag(0);

        Mode mode29=new Mode();
        mode29.setIndex(1);
        mode29.setTitle("Работа");

        Mode mode30=new Mode();
        mode30.setIndex(2);
        mode30.setTitle("Отключен");

        ZwsType type14=new ZwsType();
        type14.setId(13);
        type14.setTitle("Вспомогательный участок");
        type14.setGraphType(GraphType.POINT);
        type14.setTag(1);

        Mode mode31=new Mode();
        mode31.setIndex(1);
        mode31.setTitle("Для ЦТП");
        mode31.setSwitchState(true);

        Mode mode32=new Mode();
        mode32.setIndex(2);
        mode32.setTitle("казатель узла измерения регулятора");
        mode32.setSwitchState(false);


        type2.getModelist().add(mode1);
        type2.getModelist().add(mode2);
        type3.getModelist().add(mode3);
        type3.getModelist().add(mode4);
        type3.getModelist().add(mode5);
        type3.getModelist().add(mode6);
        type3.getModelist().add(mode7);
        type4.getModelist().add(mode8);
        type4.getModelist().add(mode9);
        type5.getModelist().add(mode10);
        type6.getModelist().add(mode11);
        type6.getModelist().add(mode12);
        type7.getModelist().add(mode13);
        type7.getModelist().add(mode14);
        type7.getModelist().add(mode15);
        type7.getModelist().add(mode16);
        type8.getModelist().add(mode17);
        type8.getModelist().add(mode18);
        type8.getModelist().add(mode19);
        type8.getModelist().add(mode20);
        type8.getModelist().add(mode21);
        type8.getModelist().add(mode22);
        type8.getModelist().add(mode23);
        type8.getModelist().add(mode24);
        type9.getModelist().add(mode25);
        type10.getModelist().add(mode26);
        type12.getModelist().add(mode27);
        type12.getModelist().add(mode28);
        type13.getModelist().add(mode29);
        type13.getModelist().add(mode30);
        type14.getModelist().add(mode31);
        type14.getModelist().add(mode32);
        type11.getModelist().add(mode33);


        GetLayerTypes getLayerTypes=new GetLayerTypes();
        getLayerTypes.getType().add(type1);
        getLayerTypes.getType().add(type2);
        getLayerTypes.getType().add(type3);
        getLayerTypes.getType().add(type4);
        getLayerTypes.getType().add(type5);
        getLayerTypes.getType().add(type6);
        getLayerTypes.getType().add(type7);
        getLayerTypes.getType().add(type8);
        getLayerTypes.getType().add(type9);
        getLayerTypes.getType().add(type10);
        getLayerTypes.getType().add(type11);
        getLayerTypes.getType().add(type12);
        getLayerTypes.getType().add(type13);
        getLayerTypes.getType().add(type14);

        return getLayerTypes;
    }
}
