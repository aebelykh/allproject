package getLayerTypes;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType(propOrder = {"id","title","graphType","tag", "modelist"})
public class ZwsType {
    private long id;
    private String title;
    private GraphType graphType = GraphType.LINE;
    private int tag;
    private List<Mode> modelist = new ArrayList<>();


    public ZwsType() {
    }

    public ZwsType(long id, String title, GraphType graphType, int tag, List<Mode> modelist) {
        this.id = id;
        this.title = title;
        this.graphType = graphType;
        this.tag = tag;
        this.modelist = modelist;
    }


    public long getId() {
        return id;
    }

    @XmlElement(name = "Id")
    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    @XmlElement(name = "Title")
    public void setTitle(String title) {
        this.title = title;
    }

    public GraphType getGraphType() {
        return graphType;
    }

    @XmlElement(name = "GraphType")
    public void setGraphType(GraphType graphType) {
        this.graphType = graphType;
    }

    public int getTag() {
        return tag;
    }

    @XmlElement(name = "Tag")
    public void setTag(int tag) {
        this.tag = tag;
    }

    @XmlElementWrapper(name = "Modes")
    public List<Mode> getModelist() {
        return modelist;
    }
    @XmlElement(name = "Mode")
    public void setModelist(List<Mode> modeslist) {
        this.modelist = modeslist;
    }
}
