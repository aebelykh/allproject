package getZMMapList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType
public class GetZMMapList {
    private List<ZwsMap> maps = new ArrayList<>();

    public GetZMMapList() {
    }

    public GetZMMapList(List<ZwsMap> maps) {
        this.maps = maps;
    }

    @XmlElement(name = "Map")
    public List<ZwsMap> getMaps() {
        return maps;
    }

    public void setMaps(List<ZwsMap> maps) {
        this.maps = maps;
    }
}
