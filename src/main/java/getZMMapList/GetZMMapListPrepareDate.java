package getZMMapList;

import zulu.ZwsResponse;

public class GetZMMapListPrepareDate {
    public static void main(String[] args) {

    }

    public static void  prepareDate(){
        ZwsMap zwsMap = new ZwsMap();
        zwsMap.setId("1cf221d3-fd7e-4c2d-a2cf-15b39d5868ac");
        zwsMap.setName("Riga");

        ZwsMap zwsMap1 = new ZwsMap();
        zwsMap1.setId("2e2d0599-2d9d-477d-a3d7-3a98f112ca0a");
        zwsMap1.setName("Демо карта");

        ZwsMap zwsMap2 = new ZwsMap();
        zwsMap2.setId("423e0266-88ec-4097-8ea0-8404001f5ee9");
        zwsMap2.setName("Новая карта");

        ZwsMap zwsMap3 = new ZwsMap();
        zwsMap3.setId("7f0dd42b-a7a2-4fd0-b3ca-2cf7f0c65dc7");
        zwsMap3.setName("buve+kreisajs");

        ZwsMap zwsMap4 = new ZwsMap();
        zwsMap4.setId("d01b395f-2efa-4a83-9a71-e907ea2b571f");
        zwsMap4.setName("Новая карта 1");

        ZwsMap zwsMap5 = new ZwsMap();
        zwsMap5.setId("f8dd3c1f-602b-4055-b588-598ecc39763e");
        zwsMap5.setName("Пример карты МО");

        GetZMMapList getZMMapList = new GetZMMapList();
        getZMMapList.getMaps().add(zwsMap);
        getZMMapList.getMaps().add(zwsMap1);
        getZMMapList.getMaps().add(zwsMap2);
        getZMMapList.getMaps().add(zwsMap3);
        getZMMapList.getMaps().add(zwsMap4);
        getZMMapList.getMaps().add(zwsMap5);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(getZMMapList);
        zwsResponse.setRetVal(6);

    }
}
