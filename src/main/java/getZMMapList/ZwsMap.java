package getZMMapList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"name","id"})
public class ZwsMap {
    private String name;
    private String id;

    public ZwsMap() {
    }

    public ZwsMap(String name, String id) {
        this.name = name;
        this.id = id;
    }


    @XmlElement(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    @XmlElement(name = "Id")
    public void setId(String id) {
        this.id = id;
    }
}
