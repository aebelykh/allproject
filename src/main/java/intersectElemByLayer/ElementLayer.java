package intersectElemByLayer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class ElementLayer {
    private int elemId;
    private int keys;

    public ElementLayer() {
    }

    public ElementLayer(int elemId, int keys) {
        this.elemId = elemId;
        this.keys = keys;
    }

    public int getElemId() {
        return elemId;
    }

    @XmlElement(name = "ElemID")
    public void setElemId(int elemId) {
        this.elemId = elemId;
    }

    public int getKeys() {
        return keys;
    }

    @XmlElement(name = "Keys")
    public void setKeys(int keys) {
        this.keys = keys;
    }
}
