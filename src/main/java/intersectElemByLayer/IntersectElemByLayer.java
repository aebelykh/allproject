package intersectElemByLayer;

import getElemsbyId.Element;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType
public class IntersectElemByLayer {
    private List<ElementLayer> elementLayers = new ArrayList<>();

    public IntersectElemByLayer() {
    }

    public IntersectElemByLayer(List<ElementLayer> elementLayers) {
        this.elementLayers = elementLayers;
    }


    @XmlElementWrapper(name = "Elements")
    public List<ElementLayer> getElementLayers() {
        return elementLayers;
    }

    @XmlElement(name = "Element")
    public void setElementLayers(List<ElementLayer> elementLayers) {
        this.elementLayers = elementLayers;
    }
}
