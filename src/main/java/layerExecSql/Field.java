package layerExecSql;

import javax.xml.bind.annotation.XmlElement;

public class Field {
    private String name;
    private String value;

    public Field() {
    }

    public Field(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @XmlElement(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    @XmlElement(name = "Value")
    public void setValue(String value) {
        this.value = value;
    }
}
