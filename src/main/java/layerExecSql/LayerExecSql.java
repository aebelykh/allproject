package layerExecSql;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType(name = "LayerExecSql")
public class LayerExecSql {
    private List<Record> records = new ArrayList<>();

    public LayerExecSql() {
    }

    public LayerExecSql(List<Record> records) {
        this.records = records;
    }
    @XmlElementWrapper(name = "Records")
    public List<Record> getRecords() {
        return records;
    }
    @XmlElement(name = "Record")
    public void setRecords(List<Record> records) {
        this.records = records;
    }
}
