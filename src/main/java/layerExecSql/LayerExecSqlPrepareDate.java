package layerExecSql;

public class LayerExecSqlPrepareDate {
    public static void main(String[] args) {

    }

    public static void prepareDate(){
        Field field = new  Field();
        field.setName("Sys");
        field.setValue("143");

        Field field1 = new Field();
        field1.setName("Geometry");
        field1.setValue("POINT(56.95237683334503 24.03602916212978)");

        Field field2 = new Field();
        field2.setName("Sys");
        field2.setValue("3378");

        Field field3 = new Field();
        field3.setName("Geometry");
        field3.setValue("LINESTRING(56.96114198632733 24.03543016538373,56.96118286253028\n" +
                "24.03478607840612,56.96093337208149 24.03452454676196,56.96103605433626\n" +
                "24.03419624903532,56.96096206652768 24.03373047962202,56.96044459706884\n" +
                "24.03243668846333)");

        Record record = new Record();
        record.getFields().add(field);
        record.getFields().add(field1);
        record.getFields().add(field2);
        record.getFields().add(field3);

        LayerExecSql layerExecSql = new LayerExecSql();
        layerExecSql.getRecords().add(record);

    }
}
