package layerExecSql;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType
public class Record {
    private List<Field> fields = new ArrayList<>();

    public Record() {
    }

    public Record(List<Field> fields) {
        this.fields = fields;
    }

    @XmlElement(name = "Field")
    public List<Field> getFields() {
        return fields;
    }

    public void setFields(List<Field> fields) {
        this.fields = fields;
    }
}
