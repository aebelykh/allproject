package layerFindConnected;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "LayerFindConnected")
public class LayerFindConnected {
    private String keys;

    public LayerFindConnected() {
    }

    public LayerFindConnected(String keys) {
        this.keys = keys;
    }

    @XmlElement(name = "Keys")
    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }
}
