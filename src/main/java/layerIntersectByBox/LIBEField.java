package layerIntersectByBox;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"name","userName","type","key","readOnly","value","url"})
public class LIBEField {
    private String name;
    private String userName;
    private String type;
    private KeyLIBB key = KeyLIBB.TRUE;
    private ReadOnlyLIBB readOnly = ReadOnlyLIBB.TRUE;
    private String Value;
    private String url;

    public LIBEField() {
    }

    public LIBEField(String name, String userName, String type, KeyLIBB key, ReadOnlyLIBB readOnly, String value, String url) {
        this.name = name;
        this.userName = userName;
        this.type = type;
        this.key = key;
        this.readOnly = readOnly;
        Value = value;
        this.url = url;
    }

    @XmlElement(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "UserName")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @XmlElement(name = "Type")
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @XmlElement(name = "Key")
    public KeyLIBB getKey() {
        return key;
    }

    public void setKey(KeyLIBB key) {
        this.key = key;
    }

    @XmlElement(name = "ReadOnly")
    public ReadOnlyLIBB getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(ReadOnlyLIBB readOnly) {
        this.readOnly = readOnly;
    }

    @XmlElement(name = "Value")
    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    @XmlElement(name = "URL")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
