package layerIntersectByBox;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType(propOrder = {"baseID","queryName","libeFields"})
public class LIBERecords {
    private String baseID;
    private String queryName;
    private List<LIBEField> libeFields = new ArrayList<>();

    public LIBERecords() {
    }

    public LIBERecords(String baseID, String queryName, List<LIBEField> libeFields) {
        this.baseID = baseID;
        this.queryName = queryName;
        this.libeFields = libeFields;
    }


    @XmlElement(name = "BaseID")
    public String getBaseID() {
        return baseID;
    }

    public void setBaseID(String baseID) {
        this.baseID = baseID;
    }

    @XmlElement(name = "QueryName")
    public String getQueryName() {
        return queryName;
    }

    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

    @XmlElementWrapper(name = "Record")
    public List<LIBEField> getLibeFields() {
        return libeFields;
    }

    @XmlElement(name = "Field")
    public void setLibeFields(List<LIBEField> libeFields) {
        this.libeFields = libeFields;
    }
}
