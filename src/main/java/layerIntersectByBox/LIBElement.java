package layerIntersectByBox;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType(propOrder = {"elemID","typeID","modeNum","libeRecords"})
public class LIBElement {
    private int elemID;
    private int typeID;
    private int modeNum;
    private List<LIBERecords> libeRecords = new ArrayList<>();

    public LIBElement() {
    }

    public LIBElement(int elemID, int typeID, int modeNum, List<LIBERecords> libeRecords) {
        this.elemID = elemID;
        this.typeID = typeID;
        this.modeNum = modeNum;
        this.libeRecords = libeRecords;
    }

    @XmlElement(name = "ElemID")
    public int getElemID() {
        return elemID;
    }

    public void setElemID(int elemID) {
        this.elemID = elemID;
    }

    @XmlElement(name = "TypeID")
    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    @XmlElement(name = "ModeNum")
    public int getModeNum() {
        return modeNum;
    }

    public void setModeNum(int modeNum) {
        this.modeNum = modeNum;
    }

    @XmlElement(name = "Records")
    public List<LIBERecords> getLibeRecords() {
        return libeRecords;
    }

    public void setLibeRecords(List<LIBERecords> libeRecords) {
        this.libeRecords = libeRecords;
    }
}
