package layerIntersectByBox;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType()
public class LayerIntersectByBox {
    private List<LIBElement> LIBElement = new ArrayList<>();

    public LayerIntersectByBox() {
    }

    public LayerIntersectByBox(List<layerIntersectByBox.LIBElement> LIBElement) {
        this.LIBElement = LIBElement;
    }

    @XmlElement(name = "Element")
    public List<layerIntersectByBox.LIBElement> getLIBElement() {
        return LIBElement;
    }

    public void setLIBElement(List<layerIntersectByBox.LIBElement> LIBElement) {
        this.LIBElement = LIBElement;
    }
}
