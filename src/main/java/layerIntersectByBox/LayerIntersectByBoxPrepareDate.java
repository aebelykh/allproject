package layerIntersectByBox;


public class LayerIntersectByBoxPrepareDate {
    public static void main(String[] args) {

    }

    public static void prepareDate(){
        //
        LIBElement libElement = new LIBElement();
        libElement.setElemID(75729);
        libElement.setTypeID(-1);
        libElement.setModeNum(-1);

        LIBERecords libeRecords = new LIBERecords();
        libeRecords.setBaseID("13");
        libeRecords.setQueryName("Запрос1");

        LIBEField libeField = new LIBEField();
        libeField.setName("Sys");
        libeField.setUserName("Sys");
        libeField.setType("integer");
        libeField.setKey(KeyLIBB.TRUE);
        libeField.setReadOnly(ReadOnlyLIBB.TRUE);
        libeField.setValue("75729");

        LIBEField libeField1 = new LIBEField();
        libeField1.setName("param1");
        libeField1.setUserName("param1");
        libeField1.setType("string");
        libeField1.setValue("абвгд");

        LIBEField libeField2 = new LIBEField();
        libeField2.setName("param2");
        libeField2.setUserName("param2");
        libeField2.setType("float");
        libeField2.setValue("12345");

        LIBEField libeField3 = new LIBEField();
        libeField3.setName("");
        libeField3.setUserName("");
        libeField3.getValue();
        libeField3.setUrl("http://zs.zulugis.ru:6473/zws/GetElemBlob/riga%3Ateplo/75729_Sys:75729/blob1/data.jpg?\n" +
                "                             BaseID=13&amp;QueryName=%D0%97%D0%B0%D0%BF%D1%80%D0%BE%D1%811");


        //
        LIBElement libElement1 = new LIBElement();
        libElement1.setElemID(75802);
        libElement1.setTypeID(14);
        libElement1.setModeNum(1);

        LIBERecords libeRecords1 = new LIBERecords();

        LIBEField libeField4 = new LIBEField();
        libeField4.setKey(KeyLIBB.TRUE);
        libeField4.setReadOnly(ReadOnlyLIBB.TRUE);
        libeField4.setName("Sys");
        libeField4.setUserName("Sys");
        libeField4.setValue("75802");

        //
        LIBElement libElement2 = new LIBElement();
        libElement2.setElemID(75803);
        libElement2.setTypeID(15);
        libElement2.setModeNum(1);

        LIBERecords libeRecords2 = new LIBERecords();

        LIBEField libeField5 = new LIBEField();
        libeField5.setKey(KeyLIBB.TRUE);
        libeField5.setReadOnly(ReadOnlyLIBB.TRUE);
        libeField5.setName("Sys");
        libeField5.setUserName("Sys");
        libeField5.setValue("75803");

        libElement.getLibeRecords().add(libeRecords1);
        libElement1.getLibeRecords().add(libeRecords1);
        libElement2.getLibeRecords().add(libeRecords2);

        libeRecords.getLibeFields().add(libeField);
        libeRecords.getLibeFields().add(libeField1);
        libeRecords.getLibeFields().add(libeField2);
        libeRecords.getLibeFields().add(libeField3);
        libeRecords1.getLibeFields().add(libeField4);
        libeRecords2.getLibeFields().add(libeField5);


    }


}
