package layerIntersectByRadius;

import zulu.ZwsResponseType;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType
public class LayerIntersectByRadius extends ZwsResponseType {
    private List<ZwsElement> elements = new ArrayList<>();

    public LayerIntersectByRadius() {
    }

    public LayerIntersectByRadius(List<ZwsElement> elements) {
        this.elements = elements;
    }

    @XmlElement(name = "Element")
    public List<ZwsElement> getElements() {
        return elements;
    }

    public void setElements(List<ZwsElement> elements) {
        this.elements = elements;
    }
}


