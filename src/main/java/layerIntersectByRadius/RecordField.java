package layerIntersectByRadius;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"name","userName","type","key","readOnly","value","URL"})
public class RecordField {
    private String Name;
    private String UserName;
    private Type type = Type.INTEGER;
    private KeyLIBR key = KeyLIBR.TRUE;
    private String URL;
    private ReadOnlyLIBR readOnly = ReadOnlyLIBR.TRUE;
    private String Value;

    public RecordField() {
    }

    public RecordField(String name, String userName, Type type, KeyLIBR key, String URL, ReadOnlyLIBR readOnly, String value) {
        Name = name;
        UserName = userName;
        this.type = type;
        this.key = key;
        this.URL = URL;
        this.readOnly = readOnly;
        Value = value;
    }
    @XmlElement(name = "Name")
    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getUserName() {
        return UserName;
    }

    @XmlElement(name = "UserName")
    public void setUserName(String userName) {
        UserName = userName;
    }

    public Type getType() {
        return type;
    }

    @XmlElement(name = "Type")
    public void setType(Type type) {
        this.type = type;
    }

    public KeyLIBR getKey() {
        return key;
    }

    @XmlElement(name = "Key")
    public void setKey(KeyLIBR key) {
        this.key = key;
    }

    public String getURL() {
        return URL;
    }

    @XmlElement(name = "URL")
    public void setURL(String URL) {
        this.URL = URL;
    }

    @XmlElement(name = "ReadOnly")
    public ReadOnlyLIBR getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(ReadOnlyLIBR readOnly) {
        this.readOnly = readOnly;
    }

    public String getValue() {
        return Value;
    }

    @XmlElement(name = "Value")
    public void setValue(String value) {
        Value = value;
    }
}
