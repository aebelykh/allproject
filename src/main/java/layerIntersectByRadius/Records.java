package layerIntersectByRadius;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType(propOrder = {"baseID","queryName","recordField"})
public class Records {
    private String BaseID;
    private String QueryName;
    private List<RecordField> recordField = new ArrayList<>();

    public Records() {
    }

    public Records(String baseID, String queryName, List<RecordField> recordField) {
        BaseID = baseID;
        QueryName = queryName;
        this.recordField = recordField;
    }


    public String getBaseID() {
        return BaseID;
    }

    @XmlElement(name = "BaseID")
    public void setBaseID(String baseID) {
        BaseID = baseID;
    }

    public String getQueryName() {
        return QueryName;
    }

    @XmlElement(name = "QueryName")
    public void setQueryName(String queryName) {
        QueryName = queryName;
    }

    @XmlElementWrapper(name = "Record")
    @XmlElement(name = "Field")
    public List<RecordField> getRecordField() {
        return recordField;
    }

    public void setRecordField(List<RecordField> recordField) {
        this.recordField = recordField;
    }
}
