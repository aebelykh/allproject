package layerIntersectByRadius;

public class StartLayerIntersectByRadius {
    public static void main(String[] args) {
    }

    public static LayerIntersectByRadius prepareDate(){
        ZwsElement element1 = new ZwsElement();
        element1.setElemID(75729);
        element1.setTypeID(-1);
        element1.setModeNum(-1);

        Records records1 = new Records();
        records1.setBaseID("13");
        records1.setQueryName("Запрос1");

        RecordField recordField1 = new RecordField();
        recordField1.setName("Sys");
        recordField1.setUserName("Sys");
        recordField1.setType(Type.INTEGER);
        recordField1.setKey(KeyLIBR.TRUE);
        recordField1.setReadOnly(ReadOnlyLIBR.TRUE);
        recordField1.setValue("75729");

        RecordField recordField2 = new RecordField();
        recordField2.setName("param1");
        recordField2.setUserName("param1");
        recordField2.setType(Type.STRING);
        recordField2.setValue("абвгд");

        RecordField recordField3 = new RecordField();
        recordField3.setName("param2");
        recordField3.setUserName("param2");
        recordField3.setType(Type.FLOAT);
        recordField3.setValue("12345");

        RecordField recordField4 = new RecordField();
        recordField4.setName("blob1");
        recordField4.setUserName("blob1");
        recordField4.setType(Type.BLOB);
        recordField4.setURL("http://zs.zulugis.ru:6473/zws/GetElemBlob/riga%3Ateplo/75729_Sys:75729/blob1/data.jpg? " +
                "BaseID=13&amp;QueryName=%D0%97%D0%B0%D0%BF%D1%80%D0%BE%D1%811");

        ZwsElement element2 = new ZwsElement();
        element2.setElemID(75802);
        element2.setTypeID(14);
        element2.setModeNum(1);

        Records records2 = new Records();

        RecordField recordField5 = new RecordField();
        recordField5.setKey(KeyLIBR.TRUE);
        recordField5.setReadOnly(ReadOnlyLIBR.TRUE);
        recordField5.setName("Sys");
        recordField5.setUserName("Sys");
        recordField5.setValue("75802");

        ZwsElement element3 = new ZwsElement();
        element3.setElemID(75803);
        element3.setTypeID(15);
        element3.setModeNum(1);

        Records records3 = new Records();

        RecordField recordField6 = new RecordField();
        recordField6.setKey(KeyLIBR.TRUE);
        recordField6.setReadOnly(ReadOnlyLIBR.TRUE);
        recordField6.setName("Sys");
        recordField6.setUserName("Sys");
        recordField6.setValue("75803");

        LayerIntersectByRadius layerIntersectByRadius = new LayerIntersectByRadius();
        layerIntersectByRadius.getElements().add(element1);
        layerIntersectByRadius.getElements().add(element2);
        layerIntersectByRadius.getElements().add(element3);

        element1.getRecord().add(records1);
        element1.getRecord().add(records2);
        element3.getRecord().add(records3);

        records1.getRecordField().add(recordField1);
        records1.getRecordField().add(recordField2);
        records1.getRecordField().add(recordField3);
        records1.getRecordField().add(recordField4);
        records2.getRecordField().add(recordField5);
        records3.getRecordField().add(recordField6);

        return layerIntersectByRadius;
    }
}
