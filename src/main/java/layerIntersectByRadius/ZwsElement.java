package layerIntersectByRadius;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


@XmlType(propOrder = {"elemID","typeID","modeNum","record"})
public class ZwsElement {
    private int ElemID;
    private int TypeID;
    private int ModeNum;
    private List<Records> record = new ArrayList<>();


    public ZwsElement() {
    }

    public ZwsElement(int elemID, int typeID, int modeNum, List<Records> record) {
        ElemID = elemID;
        TypeID = typeID;
        ModeNum = modeNum;
        this.record = record;
    }


    public int getElemID() {
        return ElemID;
    }

    @XmlElement(name = "ElemID")
    public void setElemID(int elemID) {
        ElemID = elemID;
    }

    public int getTypeID() {
        return TypeID;
    }

    @XmlElement(name = "TypeID")
    public void setTypeID(int typeID) {
        TypeID = typeID;
    }

    public int getModeNum() {
        return ModeNum;
    }

    @XmlElement(name = "ModeNum")
    public void setModeNum(int modeNum) {
        ModeNum = modeNum;
    }

    @XmlElement(name = "Records")
    public List<Records> getRecord() {
        return record;
    }

    public void setRecord(List<Records> record) {
        this.record = record;
    }
}
