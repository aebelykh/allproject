package layerQueryByExample;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class LQField {
    private String name;
    private String userName;
    private Key key = Key.TRUE;
    private ReadOnly readOnly = ReadOnly.TRUE;
    private String value;
    private String url;

    public LQField() {
    }

    public LQField(String name, String userName, Key key, ReadOnly readOnly, String value, String url) {
        this.name = name;
        this.userName = userName;
        this.key = key;
        this.readOnly = readOnly;
        this.value = value;
        this.url = url;
    }

    @XmlElement(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserName() {
        return userName;
    }

    @XmlElement(name = "UserName")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Key getKey() {
        return key;
    }

    @XmlElement(name = "KeyLIBB")
    public void setKey(Key key) {
        this.key = key;
    }

    public ReadOnly getReadOnly() {
        return readOnly;
    }

    @XmlElement(name = "ReadOnlyLIBB")
    public void setReadOnly(ReadOnly readOnly) {
        this.readOnly = readOnly;
    }

    @XmlElement(name = "Value")
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @XmlElement(name = "Url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
