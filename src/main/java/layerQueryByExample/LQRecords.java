package layerQueryByExample;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType(propOrder = {"baseID","userName","queryName","lqFields"})
public class LQRecords {
    private int baseID;
    private String userName;
    private String queryName;
    private List<LQField> lqFields = new ArrayList<>();


    public LQRecords() {
    }

    public LQRecords(int baseID, String userName, String queryName, List<LQField> lqFields) {
        this.baseID = baseID;
        this.userName = userName;
        this.queryName = queryName;
        this.lqFields = lqFields;
    }


    @XmlElement(name = "BaseID")
    public int getBaseID() {
        return baseID;
    }

    public void setBaseID(int baseID) {
        this.baseID = baseID;
    }

    @XmlElement(name = "UserName")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @XmlElement(name = "QueryName")
    public String getQueryName() {
        return queryName;
    }

    public void setQueryName(String queryName) {
        this.queryName = queryName;
    }

    @XmlElementWrapper(name = "Record")
    public List<LQField> getLqFields() {
        return lqFields;
    }


    @XmlElement(name = "Field")
    public void setLqFields(List<LQField> lqFields) {
        this.lqFields = lqFields;
    }
}
