package layerQueryByExample;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType(name = "LayerQueryByExample", propOrder = {"lqRecords","recordCount"})
public class LayerQueryByExample {
    private List<LQRecords> lqRecords = new ArrayList<>();
    private int recordCount;

    public LayerQueryByExample() {
    }

    public LayerQueryByExample(List<LQRecords> lqRecords, int recordCount) {
        this.lqRecords = lqRecords;
        this.recordCount = recordCount;
    }

    public LayerQueryByExample(List<LQRecords> lqRecords) {
        this.lqRecords = lqRecords;
    }

    @XmlElement(name = "Records")
    public List<LQRecords> getLqRecords() {
        return lqRecords;
    }

    public void setLqRecords(List<LQRecords> lqRecords) {
        this.lqRecords = lqRecords;
    }

    @XmlElement(name = "RecordCount")
    public int getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(int recordCount) {
        this.recordCount = recordCount;
    }
}
