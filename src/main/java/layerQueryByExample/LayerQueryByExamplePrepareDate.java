package layerQueryByExample;

public class LayerQueryByExamplePrepareDate {
    public static void main(String[] args) {

    }

    public static void prepareDate(){
        LQRecords lqRecords = new LQRecords();
        lqRecords.setBaseID(2);
        lqRecords.setUserName("Тепловая камера");
        lqRecords.setQueryName("Основной");

        LQField lqField = new LQField();
        lqField.setName("");
        lqField.setUserName("");
        lqField.setKey(Key.TRUE);
        lqField.setReadOnly(ReadOnly.TRUE);
        lqField.setValue("9");

        LQField lqField1 = new LQField();
        lqField1.setName("Nist");
        lqField1.setUserName("Номер источника");
        lqField1.setValue("4");

        LQField lqField2 = new LQField();
        lqField2.setName("Siltumapgades zona");
        lqField2.setUserName("Siltumapgades zona");
        lqField2.setValue("I");

        LQField lqField3 = new LQField();
        lqField3.setName("Blob1");
        lqField3.setUserName("Blob1");
        lqField3.getValue();
        lqField3.getUrl();

        LQField lqField4 = new LQField();
        lqField4.setName("DateTime1");
        lqField4.setUserName("DateTime1");
        lqField4.getValue();

        LQField lqField5 = new LQField();
        lqField5.setName("Date1");
        lqField5.setUserName("Date1");
        lqField5.getValue();

        LQField lqField6 = new LQField();
        lqField6.setName("Time1");
        lqField6.setUserName("Time1");
        lqField6.getValue();

        LQField lqField7 = new LQField();
        lqField7.setName("TR");
        lqField7.setUserName("TR");
        lqField7.setValue("2");

        LQField lqField8 = new LQField();
        lqField8.setName("Kat");
        lqField8.setUserName("Kategorija");
        lqField8.setValue("M");

        LQField lqField9 = new LQField();
        lqField9.setName("Pied");
        lqField9.setUserName("Piederiba");
        lqField9.setValue("RS");

        LQField lqField10 = new LQField();
        lqField10.setName("Pied");
        lqField10.setUserName("Piederiba");
        lqField10.setValue("RS");

        LQField lqField11 = new LQField();
        lqField11.setName("Pied");
        lqField11.setUserName("Piederiba");
        lqField11.setValue("RS");

        LQField lqField12 = new LQField();
        lqField12.setName("Pied");
        lqField12.setUserName("Piederiba");
        lqField12.setValue("RS");

        LQField lqField13 = new LQField();
        lqField13.setName("Pied");
        lqField13.setUserName("Piederiba");
        lqField13.setValue("RS");

        LQField lqField14 = new LQField();
        lqField14.setName("Shema");
        lqField14.setUserName("Shema");
        lqField14.setUrl("http://zs.zulugis.ru:6473/zws/GetElemBlob/riga%3Ateplo/9_Sys:9/Shema/data.jpg?\n" +
                "                         BaseID=2&amp;QueryName=%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D0%BD%D0%BE%D0%B9");


        LQField lqField15 = new LQField();
        lqField15.setName("H_geo");
        lqField15.setUserName("Геодезическая отметка, м");
        lqField15.setValue("9");

        LQField lqField16 = new LQField();
        lqField16.setName("K_ism");
        lqField16.setUserName("K_ism");
        lqField16.setValue("D");

        LQField lqField17 = new LQField();
        lqField17.setName("Jeg_dzil");
        lqField17.setUserName("Jeguldisanas dzilums");
        lqField17.getValue();

        LQField lqField18 = new LQField();
        lqField18.setName("Izb_v");
        lqField18.setUserName("Izbuves veids");
        lqField18.getValue();

        LQField lqField19 = new LQField();
        lqField19.setName("Izb_g");
        lqField19.setUserName("Izbuves gads");
        lqField19.getValue();

        LQField lqField20 = new LQField();
        lqField20.setName("Sig");
        lqField20.setUserName("Signalizacija");
        lqField20.getValue();

        LQField lqField21 = new LQField();
        lqField21.setName("Sig_sta");
        lqField21.setUserName("Signalizacija stavoklis");
        lqField21.getValue();

        LQField lqField22 = new LQField();
        lqField22.setName("H_pod");
        lqField22.setUserName("Напор в подающем трубопроводе, м");
        lqField22.setValue("83.997");

        LQField lqField23 = new LQField();
        lqField23.setName("H_obr");
        lqField23.setUserName("Напоp в обpатном тpубопpоводе, м");
        lqField23.setValue("29.003");

        LQField lqField24 = new LQField();
        lqField24.setName("Tb");
        lqField24.setUserName("Давление вскипания, м");
        lqField24.setValue("-6.34");

        LQField lqField25 = new LQField();
        lqField25.setName("Hstat");
        lqField25.setUserName("Статический напор, м");
        lqField25.setValue("31");

        LQField lqField26 = new LQField();
        lqField26.setName("Hstat_out");
        lqField26.setUserName("Статический напор на выходе, м");
        lqField26.setValue("31");

        LQField lqField27 = new LQField();
        lqField27.setName("Gpod");
        lqField27.setUserName("Слив из подающего трубопровода, т/ч");
        lqField27.getValue();

        LQField lqField28 = new LQField();
        lqField28.setName("Gobr");
        lqField28.setUserName("Слив из обратного трубопровода, т/ч");
        lqField28.getValue();

        LayerQueryByExample layerQueryByExample = new LayerQueryByExample();
        layerQueryByExample.getLqRecords().add(lqRecords);

        lqRecords.getLqFields().add(lqField);
        lqRecords.getLqFields().add(lqField1);
        lqRecords.getLqFields().add(lqField2);
        lqRecords.getLqFields().add(lqField3);
        lqRecords.getLqFields().add(lqField4);
        lqRecords.getLqFields().add(lqField5);
        lqRecords.getLqFields().add(lqField6);
        lqRecords.getLqFields().add(lqField7);
        lqRecords.getLqFields().add(lqField8);
        lqRecords.getLqFields().add(lqField9);
        lqRecords.getLqFields().add(lqField10);
        lqRecords.getLqFields().add(lqField11);
        lqRecords.getLqFields().add(lqField12);
        lqRecords.getLqFields().add(lqField13);
        lqRecords.getLqFields().add(lqField14);
        lqRecords.getLqFields().add(lqField15);
        lqRecords.getLqFields().add(lqField16);
        lqRecords.getLqFields().add(lqField17);
        lqRecords.getLqFields().add(lqField18);
        lqRecords.getLqFields().add(lqField19);
        lqRecords.getLqFields().add(lqField20);
        lqRecords.getLqFields().add(lqField21);
        lqRecords.getLqFields().add(lqField22);
        lqRecords.getLqFields().add(lqField23);
        lqRecords.getLqFields().add(lqField24);
        lqRecords.getLqFields().add(lqField25);
        lqRecords.getLqFields().add(lqField26);
        lqRecords.getLqFields().add(lqField27);
        lqRecords.getLqFields().add(lqField28);


    }
}
