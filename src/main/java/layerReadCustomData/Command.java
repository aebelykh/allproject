package layerReadCustomData;



import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class Command {
    private LayerReadCustomData layerReadCustomData = new LayerReadCustomData();

    public Command() {
    }

    public Command(LayerReadCustomData layerReadCustomData) {
        this.layerReadCustomData = layerReadCustomData;
    }


    @XmlElement(name = "LayerReadCustomData")
    public LayerReadCustomData getLayerReadCustomData() {
        return layerReadCustomData;
    }

    public void setLayerReadCustomData(LayerReadCustomData layerReadCustomData) {
        this.layerReadCustomData = layerReadCustomData;
    }
}
