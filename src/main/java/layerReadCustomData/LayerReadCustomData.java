package layerReadCustomData;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class LayerReadCustomData {
    private String name;
    private String layer;

    public LayerReadCustomData() {
    }

    public LayerReadCustomData(String name, String layer) {
        this.name = name;
        this.layer = layer;
    }

    @XmlElement(name = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLayer() {
        return layer;
    }

    @XmlElement(name = "Layer")
    public void setLayer(String layer) {
        this.layer = layer;
    }
}
