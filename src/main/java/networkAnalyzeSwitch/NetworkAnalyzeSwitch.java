package networkAnalyzeSwitch;

import zulu.ZwsResponseType;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType(propOrder = {"elements","count"})
public class NetworkAnalyzeSwitch extends ZwsResponseType {
    private List<ZwsElemen> elements = new ArrayList<>();
    private int Count = 4;

    public NetworkAnalyzeSwitch() {
    }

    public NetworkAnalyzeSwitch(List<ZwsElemen> elements, int count) {
        this.elements = elements;
        Count = count;
    }


    @XmlElementWrapper(name = "Elements")
    public List<ZwsElemen> getElements() {
        return elements;
    }

    @XmlElement(name = "Element")
    public void setElements(List<ZwsElemen> elements) {
        this.elements = elements;
    }

    @XmlElement(name = "Count")
    public int getCount() {
        return Count;
    }

    public void setCount(int count) {
        Count = count;
    }
}
