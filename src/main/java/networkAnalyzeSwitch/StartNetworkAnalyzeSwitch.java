package networkAnalyzeSwitch;

public class StartNetworkAnalyzeSwitch {
    public static void main(String[] args) {
    }

    public static NetworkAnalyzeSwitch prepareDate(){
        ZwsElemen element1 = new ZwsElemen();
        element1.setElemID(19980);
        element1.setState(State.OFF);

        ZwsElemen element2 = new ZwsElemen();
        element2.setElemID(19998);
        element2.setState(State.OFF);

        ZwsElemen element3 = new ZwsElemen();
        element3.setElemID(20000);
        element3.setState(State.OFF);

        ZwsElemen element4 = new ZwsElemen();
        element4.setElemID(20002);
        element4.setState(State.OFF);

        NetworkAnalyzeSwitch networkAnalyzeSwitch = new NetworkAnalyzeSwitch();
        networkAnalyzeSwitch.getElements().add(element1);
        networkAnalyzeSwitch.getElements().add(element2);
        networkAnalyzeSwitch.getElements().add(element3);
        networkAnalyzeSwitch.getElements().add(element4);

        return networkAnalyzeSwitch;
    }

}
