package networkAnalyzeSwitch;

import javax.xml.bind.annotation.XmlElement;

public class ZwsElemen {
    private int ElemID;
    private State state = State.OFF;

    public ZwsElemen() {
    }

    public ZwsElemen(int elemID, State state) {
        ElemID = elemID;
        this.state = state;
    }


    public int getElemID() {
        return ElemID;
    }

    @XmlElement(name = "ElemID")
    public void setElemID(int elemID) {
        ElemID = elemID;
    }

    public State getState() {
        return state;
    }

    @XmlElement(name = "State")
    public void setState(State state) {
        this.state = state;
    }
}
