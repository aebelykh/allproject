package networkRecalc;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType
public class NetworkRecalc {
    private int count;
    private String keys;

    public NetworkRecalc() {
    }

    public NetworkRecalc(int count, String keys) {
        this.count = count;
        this.keys = keys;
    }


    @XmlElement(name = "Count")
    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @XmlElement(name = "Keys")
    public String getKeys() {
        return keys;
    }

    public void setKeys(String keys) {
        this.keys = keys;
    }
}
