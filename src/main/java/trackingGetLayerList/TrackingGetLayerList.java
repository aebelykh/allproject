package trackingGetLayerList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;
@XmlType
public class TrackingGetLayerList {
    private List<Layer> layers = new ArrayList<>();

    public TrackingGetLayerList() {
    }

    public TrackingGetLayerList(List<Layer> layers) {
        this.layers = layers;
    }

    @XmlElement(name = "Layer")
    public List<Layer> getLayers() {
        return layers;
    }

    public void setLayers(List<Layer> layers) {
        this.layers = layers;
    }
}
