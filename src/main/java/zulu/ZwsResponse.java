package zulu;

import LayerFindWay.LayerFindWay;
import LayerGetIncidentElements.LayerGetIncidentElements;
import getElemsbyId.Element;
import getElemsbyId.GetElemsByID;
import getLayerBounds.GetLayerBounds;
import getLayerLabels.GetLayerLabels;
import getLayerList.GetLayerList;
import getLayerThemes.GetLayerThemes;
import getLayerTypes.GetLayerTypes;
import getLayerUpdateCount.GetLayerUpdateCount;
import getZMMapList.GetZMMapList;
import intersectElemByLayer.IntersectElemByLayer;
import layerAddPolygon.LayerAddPolygon;
import layerAddPolyline.LayerAddPolyline;
import layerAddSymbol.LayerAddSymbol;
import layerDeleteElement.LayerDeleteElement;
import layerDeleteNode.LayerDeleteNode;
import layerExecSql.LayerExecSql;
import layerFindConnected.LayerFindConnected;
import layerInsertNode.LayerInsertNode;
import layerIntersectByBox.LayerIntersectByBox;
import layerIntersectByRadius.LayerIntersectByRadius;
import layerMoveElement.LayerMoveElement;
import layerMoveNode.LayerMoveNode;
import layerQueryByExample.LayerQueryByExample;
import layerReadCustomData.LayerReadCustomData;
import networkAnalyzeSwitch.NetworkAnalyzeSwitch;
import networkRecalc.NetworkRecalc;
import setElemState.SetElemState;
import trackingGetLayerList.TrackingGetLayerList;
import updateElemAttributes.UpdateElemAttributes;
import javax.xml.bind.annotation.*;

/**
 * Created by Kashoutin Sergey (kashoutin.s@gmail.com) on 05.12.2018
 */

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement
public class ZwsResponse<ResponseType> extends ZwsResponseType {

    private ResponseType response;
    private Integer retVal;


    public ZwsResponse() {
    }

    public ZwsResponse(ResponseType response) {
        this.response = response;
    }

    public ZwsResponse(ResponseType response, Integer retVal) {
        this.response = response;
        this.retVal = retVal;
    }

    @XmlElements(value = {
            @XmlElement(name = "GetElemsbyId", type = GetElemsByID.class),
            @XmlElement(name = "GetLayerLabels", type = GetLayerLabels.class),
            @XmlElement(name = "GetLayerList", type = GetLayerList.class),
            @XmlElement(name = "GetLayerThemes", type = GetLayerThemes.class),
            @XmlElement(name = "GetLayerTypes", type = GetLayerTypes.class),
            @XmlElement(name = "LayerIntersectByRadius", type = LayerIntersectByRadius.class),
            @XmlElement(name = "NetworkAnalyzeSwitch", type = NetworkAnalyzeSwitch.class),
            @XmlElement(name = "GetLayerBounds", type = GetLayerBounds.class),
            @XmlElement(name = "GetLayerUpdateCount", type = GetLayerUpdateCount.class),
            @XmlElement(name = "GetZMMapList",type = GetZMMapList.class),
            @XmlElement(name = "IntersectElemByLayer", type = IntersectElemByLayer.class),
            @XmlElement(name = "LayerAddPolygon", type = LayerAddPolygon.class),
            @XmlElement(name = "LayerAddPolyline", type = LayerAddPolyline.class),
            @XmlElement(name = "LayerAddSymbol", type = LayerAddSymbol.class),
            @XmlElement(name = "LayerDeleteElement", type = LayerDeleteElement.class),
            @XmlElement(name = "LayerDeleteNode", type = LayerDeleteNode.class),
            @XmlElement(name = "LayerExecSql", type = LayerExecSql.class),
            @XmlElement(name = "LayerFindConnected", type = LayerFindConnected.class),
            @XmlElement(name = "LayerFindWay", type = LayerFindWay.class),
            @XmlElement(name = "LayerGetIncidentElements", type = LayerGetIncidentElements.class),
            @XmlElement(name = "LayerIntersectByBox", type = LayerIntersectByBox.class),
            @XmlElement(name = "LayerInsertNode", type = LayerInsertNode.class),
            @XmlElement(name = "LayerMoveElement", type = LayerMoveElement.class),
            @XmlElement(name = "LayerMoveNode", type = LayerMoveNode.class),
            @XmlElement(name = "LayerQueryByExample", type = LayerQueryByExample.class),
            @XmlElement(name = "LayerReadCustomData", type = LayerReadCustomData.class),
            @XmlElement(name = "NetworkRecalc", type = NetworkRecalc.class),
            @XmlElement(name = "UpdateElemAttributes", type = UpdateElemAttributes.class),
            @XmlElement(name = "SetElemState", type = SetElemState.class),
            @XmlElement(name = "TrackingGetLayerList", type = TrackingGetLayerList.class),
    })
    public ResponseType getResponse() {
        return response;
    }

    public void setResponse(ResponseType response) {
        this.response = response;
    }

    @XmlElement(name = "RetVal")
    public Integer getRetVal() {
        return retVal;
    }

    public void setRetVal(Integer retVal) {
        this.retVal = retVal;
    }
}
