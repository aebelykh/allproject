package zulu;

/**
 * Created by Kashoutin Sergey (kashoutin.s@gmail.com) on 05.12.2018
 *
 * класс-заглушка дабы типизиорвать наследников, которых можно передвать в класс ZwsResponse<ResponseType extends ZwsResponseType>
 */
public class ZwsResponseType {
}
