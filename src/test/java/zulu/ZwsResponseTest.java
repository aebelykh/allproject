package zulu;

import LayerFindWay.LayerFindWay;
import LayerGetIncidentElements.LayerGIElement;
import LayerGetIncidentElements.LayerGetIncidentElements;
import layerInsertNode.LayerInsertNode;
import layerIntersectByBox.*;
import layerIntersectByRadius.KeyLIBR;
import networkRecalc.NetworkRecalc;
import getZMMapList.GetZMMapList;
import getZMMapList.ZwsMap;
import layerAddPolyline.LayerAddPolyline;
import getElemsbyId.*;
import getElemsbyId.Element;
import getLayerBounds.BoundingBox;
import getLayerBounds.GetLayerBounds;
import getLayerBounds.ZwsBounds;
import getLayerLabels.GetLayerLabels;
import getLayerLabels.ZwsLabelLayers;
import getLayerList.GetLayerList;
import getLayerList.ZwsList;
import getLayerThemes.GetLayerThemes;
import getLayerThemes.ZwsThemes;
import getLayerTypes.GetLayerTypes;
import getLayerTypes.GraphType;
import getLayerTypes.Mode;
import getLayerTypes.ZwsType;
import getLayerUpdateCount.GetLayerUpdateCount;
import intersectElemByLayer.ElementLayer;
import intersectElemByLayer.IntersectElemByLayer;
import layerAddPolygon.LayerAddPolygon;
import layerAddSymbol.LayerAddSymbol;
import layerDeleteElement.LayerDeleteElement;
import layerDeleteNode.LayerDeleteNode;
import layerExecSql.Field;
import layerExecSql.LayerExecSql;
import layerExecSql.Record;
import layerFindConnected.LayerFindConnected;
import layerIntersectByRadius.*;
import layerMoveElement.LayerMoveElement;
import layerMoveNode.LayerMoveNode;
import layerQueryByExample.LQField;
import layerQueryByExample.LQRecords;
import layerQueryByExample.LayerQueryByExample;
import layerReadCustomData.Command;
import layerReadCustomData.LayerReadCustomData;
import networkAnalyzeSwitch.NetworkAnalyzeSwitch;
import networkAnalyzeSwitch.State;
import networkAnalyzeSwitch.ZwsElemen;
import org.junit.Test;
import setElemState.SetElemState;
import trackingGetLayerList.Layer;
import trackingGetLayerList.TrackingGetLayerList;
import updateElemAttributes.UpdateElemAttributes;

import javax.xml.bind.*;
import java.io.StringWriter;

import static junit.framework.Assert.*;

public class ZwsResponseTest{

    public final static String XML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "    <zwsResponse>\n" +
            "        <Element>\n" +
            "            <ElemID>143</ElemID>\n" +
            "            <TypeID>2</TypeID>\n" +
            "            <ModeNum>1</ModeNum>\n" +
            "            <Geometry>\n" +
            "                <Format>KML</Format>\n" +
            "                   <Placemark>\n" +
            "                      <Point>\n" +
            "                          <coordinates>24.0360291621298,56.9523768333450</coordinates>\n" +
            "                      </Point>\n" +
            "                  </Placemark>\n" +
            "           </Geometry>\n" +
            "        </Element>\n" +
            "        <Element>\n" +
            "            <ElemID>145</ElemID>\n" +
            "            <TypeID>2</TypeID>\n" +
            "            <ModeNum>1</ModeNum>\n" +
            "            <Geometry>\n" +
            "                <Format>KML</Format>\n" +
            "                 <Placemark>\n" +
            "                      <Point>\n" +
            "                          <coordinates>24.0352832245440,56.9537345512725</coordinates>\n" +
            "                      </Point>\n" +
            "                 </Placemark>\n" +
            "            </Geometry>\n" +
            "        </Element>\n" +
            "        <Element>\n" +
            "            <ElemID>147</ElemID>\n" +
            "            <TypeID>2</TypeID>\n" +
            "            <ModeNum>1</ModeNum>\n" +
            "            <Geometry>\n" +
            "                <Format>KML</Format>\n" +
            "                <Placemark>\n" +
            "                    <Point>\n" +
            "                        <coordinates>24.0353179530160,56.9548176887342</coordinates>\n" +
            "                    </Point>\n" +
            "                </Placemark>\n" +
            "            </Geometry>\n" +
            "        </Element>\n" +
            "        <RetVal>3</RetVal>\n" +
            "    </zwsResponse>";


    @Test
    public void GetElemsbyId() throws JAXBException {
        Element element1 = new Element();
        element1.setElemID(143);
        element1.setTypeID(2);
        element1.setModeNum(1);
        element1.getGeometry().getFormat();
        element1.getGeometry().getPlacemark().getPoint().getCoordinates();

        Element element2 = new Element();
        element2.setElemID(145);
        element2.setTypeID(2);
        element2.setModeNum(1);
        element2.getGeometry().getFormat();
        element2.getGeometry().getPlacemark().getPoint().getCoordinates();

        Element element3 = new Element();
        element3.setElemID(147);
        element3.setTypeID(2);
        element3.setModeNum(1);
        element3.getGeometry().getFormat();
        element3.getGeometry().getPlacemark().getPoint().getCoordinates();


        GetElemsByID elementlist = new GetElemsByID();
        elementlist.getElements().add(element1);
        elementlist.getElements().add(element2);
        elementlist.getElements().add(element3);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(elementlist);
        zwsResponse.setRetVal(3);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class, GetElemsByID.class,Placemark.class, Point.class, Geometry.class,Element.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML,stringWriter);


    } /////////////////////////////////////////////////////////////

    @Test
    public void GetLayerLabels() throws JAXBException {
        ZwsLabelLayers labelLayer1 = new ZwsLabelLayers();
        labelLayer1.setID(0);
        labelLayer1.setUserName("Siltumavotu parametri");

        ZwsLabelLayers labelLayer2 = new ZwsLabelLayers();
        labelLayer2.setID(1);
        labelLayer2.setUserName("Siltumavoti");

        ZwsLabelLayers labelLayer3 = new ZwsLabelLayers();
        labelLayer3.setID(2);
        labelLayer3.setUserName("Piesleguma datums");

        ZwsLabelLayers labelLayer4 = new ZwsLabelLayers();
        labelLayer4.setID(3);
        labelLayer4.setUserName("Kameru nosaukumi");

        ZwsLabelLayers labelLayer5 = new ZwsLabelLayers();
        labelLayer5.setID(4);
        labelLayer5.setUserName("Aizbidni,koveru nosaukumi");

        ZwsLabelLayers labelLayer6 = new ZwsLabelLayers();
        labelLayer6.setID(5);
        labelLayer6.setUserName("Zad");

        ZwsLabelLayers labelLayer7 = new ZwsLabelLayers();
        labelLayer7.setID(6);
        labelLayer7.setUserName("Abonentu slodzes");

        ZwsLabelLayers labelLayer8 = new ZwsLabelLayers();
        labelLayer8.setID(7);
        labelLayer8.setUserName("Klients");

        ZwsLabelLayers labelLayer9 = new ZwsLabelLayers();
        labelLayer9.setID(8);
        labelLayer9.setUserName("PAC");

        ZwsLabelLayers labelLayer10 = new ZwsLabelLayers();
        labelLayer10.setID(9);
        labelLayer10.setUserName("Abonenta informacija");

        ZwsLabelLayers labelLayer11 = new ZwsLabelLayers();
        labelLayer11.setID(10);
        labelLayer11.setUserName("Kamera_dati");

        ZwsLabelLayers labelLayer12 = new ZwsLabelLayers();
        labelLayer12.setID(11);
        labelLayer12.setUserName("Участки");

        GetLayerLabels getLayerLabels = new GetLayerLabels();
        getLayerLabels.getLabelLayer().add(labelLayer1);
        getLayerLabels.getLabelLayer().add(labelLayer2);
        getLayerLabels.getLabelLayer().add(labelLayer3);
        getLayerLabels.getLabelLayer().add(labelLayer4);
        getLayerLabels.getLabelLayer().add(labelLayer5);
        getLayerLabels.getLabelLayer().add(labelLayer6);
        getLayerLabels.getLabelLayer().add(labelLayer7);
        getLayerLabels.getLabelLayer().add(labelLayer8);
        getLayerLabels.getLabelLayer().add(labelLayer9);
        getLayerLabels.getLabelLayer().add(labelLayer10);
        getLayerLabels.getLabelLayer().add(labelLayer11);
        getLayerLabels.getLabelLayer().add(labelLayer12);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(getLayerLabels);
        zwsResponse.setRetVal(12);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,GetLayerLabels.class,ZwsLabelLayers.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML,stringWriter);


    }

    @Test
    public void GetLayerList() throws JAXBException {
        ZwsList list1 = new ZwsList();
        list1.setName("riga:houses");
        list1.setTitle("buve 1");

        ZwsList list2 = new ZwsList();
        list2.setName("riga:teplo");
        list2.setTitle("Kreisajs");

        ZwsList list3 = new ZwsList();
        list3.setName("mo:vo");
        list3.setTitle("Водоотведение");

        ZwsList list4 = new ZwsList();
        list4.setName("mo:ts");
        list4.setTitle("Тепловая сеть");

        ZwsList list5 = new ZwsList();
        list5.setName("mo:region");
        list5.setTitle("Районы МО");

        ZwsList list6 = new ZwsList();
        list6.setName("kr:adm");
        list6.setTitle("Административное деление");

        GetLayerList getLayerList = new GetLayerList();
        getLayerList.getList().add(list1);
        getLayerList.getList().add(list2);
        getLayerList.getList().add(list3);
        getLayerList.getList().add(list4);
        getLayerList.getList().add(list5);
        getLayerList.getList().add(list6);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(getLayerList);
        zwsResponse.setRetVal(6);


        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(GetLayerList.class,ZwsList.class, ZwsResponse.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML,stringWriter);
    }

    @Test
    public void GetLayerThemes() throws JAXBException {
        ZwsThemes themes1 = new ZwsThemes();
        themes1.setID(1);
        themes1.setUserName("Источники");

        ZwsThemes themes2 = new ZwsThemes();
        themes2.setID(2);
        themes2.setUserName("Участки 0.9");

        GetLayerThemes getLayerThemes = new GetLayerThemes();
        getLayerThemes.getTheme().add(themes1);
        getLayerThemes.getTheme().add(themes2);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(getLayerThemes);
        zwsResponse.setRetVal(2);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class, GetLayerThemes.class, ZwsThemes.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(zwsResponse, stringWriter);

        assertEquals(XML,stringWriter);
    }

    @Test
    public void GetLayerTypes() throws JAXBException {
        ZwsType type1=new ZwsType();
        type1.setId(-1);
        type1.setTitle("Primitives");
        type1.setGraphType(null);
        type1.setModelist(null);
        type1.setTag(0);

        ZwsType type2=new ZwsType();
        type2.setId(1);
        type2.setTitle("Источник");
        type2.setGraphType(GraphType.POINT);
        type2.setTag(2);

        Mode mode1=new Mode();
        mode1.setIndex(1);
        mode1.setTitle("Работа");
        mode1.setSwitchState(true);

        Mode mode2=new Mode();
        mode2.setIndex(2);
        mode2.setTitle("Отключен");
        mode2.setSwitchState(false);

        ZwsType type3=new ZwsType();
        type3.setId(2);
        type3.setTitle("Узел");
        type3.setGraphType(GraphType.POINT);
        type3.setTag(5);

        Mode mode3=new Mode();
        mode3.setIndex(1);
        mode3.setTitle("Тепловая камера");

        Mode mode4=new Mode();
        mode4.setIndex(2);
        mode4.setTitle("Разветвление");

        Mode mode5=new Mode();
        mode5.setIndex(3);
        mode5.setTitle("Смена диаметра");

        Mode mode6=new Mode();
        mode6.setIndex(4);
        mode6.setTitle("cmena vida prok");

        Mode mode7=new Mode();
        mode7.setIndex(5);
        mode7.setTitle("Kover");

        ZwsType type4=new ZwsType();
        type4.setId(3);
        type4.setTitle("Потpебитель");
        type4.setGraphType(GraphType.POINT);
        type4.setTag(4);

        Mode mode8=new Mode();
        mode8.setIndex(1);
        mode8.setTitle("Работа");

        Mode mode9=new Mode();
        mode9.setIndex(2);
        mode9.setTitle("Отключен");

        ZwsType type5=new ZwsType();
        type5.setId(4);
        type5.setTitle("Насосная станция");
        type5.setGraphType(GraphType.POINT);
        type5.setTag(5);

        Mode mode10=new Mode();
        mode10.setIndex(1);
        mode10.setTitle("Работа");

        ZwsType type6=new ZwsType();
        type6.setId(5);
        type6.setTitle("Задвижка");
        type6.setGraphType(GraphType.POINT);
        type6.setTag(3);

        Mode mode11=new Mode();
        mode11.setIndex(1);
        mode11.setTitle("Открыта");

        Mode mode12=new Mode();
        mode12.setIndex(2);
        mode12.setTitle("Закрыта");

        ZwsType type7=new ZwsType();
        type7.setId(6);
        type7.setTitle("Участки");
        type7.setGraphType(GraphType.LINE);
        type7.setTag(1);

        Mode mode13=new Mode();
        mode13.setIndex(1);
        mode13.setTitle("Включен");
        mode13.setSwitchState(true);

        Mode mode14=new Mode();
        mode14.setIndex(2);
        mode14.setTitle("Отключен");
        mode14.setSwitchState(false);

        Mode mode15=new Mode();
        mode15.setIndex(3);
        mode15.setTitle("Отключ. обратный тp-д");
        mode15.setSwitchState(true);

        Mode mode16=new Mode();
        mode16.setIndex(4);
        mode16.setTitle("Отключ. обратный тp-д");
        mode16.setSwitchState(true);

        ZwsType type8=new ZwsType();
        type8.setId(7);
        type8.setTitle("Дросселирующий узел");
        type8.setGraphType(GraphType.POINT);
        type8.setTag(5);

        Mode mode17=new Mode();
        mode17.setIndex(1);
        mode17.setTitle("Вычиcляемая шайба");

        Mode mode18=new Mode();
        mode18.setIndex(2);
        mode18.setTitle("Устанавливаемая шайба");

        Mode mode19=new Mode();
        mode19.setIndex(3);
        mode19.setTitle("Регулятор напора");

        Mode mode20=new Mode();
        mode20.setIndex(4);
        mode20.setTitle("Регулятор давления в подаюшем");

        Mode mode21=new Mode();
        mode21.setIndex(5);
        mode21.setTitle("Регулятор давления в обратном");

        Mode mode22=new Mode();
        mode22.setIndex(6);
        mode22.setTitle("Регулятор расхода на подающем");

        Mode mode23=new Mode();
        mode23.setIndex(7);
        mode23.setTitle("Регулятор расхода на подающем");

        Mode mode24=new Mode();
        mode24.setIndex(8);
        mode24.setTitle("Регулятор напора на обратном");

        ZwsType type9=new ZwsType();
        type9.setId(8);
        type9.setTitle("ЦТП");
        type9.setGraphType(GraphType.POINT);
        type9.setTag(0);

        Mode mode25=new Mode();
        mode25.setIndex(1);
        mode25.setTitle("ЦТП");

        ZwsType type10=new ZwsType();
        type10.setId(9);
        type10.setTitle("ЦТП");
        type10.setGraphType(GraphType.POINT);
        type10.setTag(0);

        Mode mode26=new Mode();
        mode26.setIndex(1);
        mode26.setTitle("Граница");

        ZwsType type11=new ZwsType();
        type11.setId(10);
        type11.setTitle("Узел учета");
        type11.setGraphType(GraphType.POINT);
        type11.setTag(0);

        Mode mode33=new Mode();
        mode33.setIndex(1);
        mode33.setTitle("Приборы учета");

        ZwsType type12=new ZwsType();
        type12.setId(11);
        type12.setTitle("Перемычка");
        type12.setGraphType(GraphType.POINT);
        type12.setTag(0);

        Mode mode27=new Mode();
        mode27.setIndex(1);
        mode27.setTitle("Включена");

        Mode mode28=new Mode();
        mode28.setIndex(2);
        mode28.setTitle("Закрыта");

        ZwsType type13=new ZwsType();
        type13.setId(12);
        type13.setTitle("Обобщенный потребитель");
        type13.setGraphType(GraphType.POINT);
        type13.setTag(0);

        Mode mode29=new Mode();
        mode29.setIndex(1);
        mode29.setTitle("Работа");

        Mode mode30=new Mode();
        mode30.setIndex(2);
        mode30.setTitle("Отключен");

        ZwsType type14=new ZwsType();
        type14.setId(13);
        type14.setTitle("Вспомогательный участок");
        type14.setGraphType(GraphType.POINT);
        type14.setTag(1);

        Mode mode31=new Mode();
        mode31.setIndex(1);
        mode31.setTitle("Для ЦТП");
        mode31.setSwitchState(true);

        Mode mode32=new Mode();
        mode32.setIndex(2);
        mode32.setTitle("казатель узла измерения регулятора");
        mode32.setSwitchState(false);


        type2.getModelist().add(mode1);
        type2.getModelist().add(mode2);
        type3.getModelist().add(mode3);
        type3.getModelist().add(mode4);
        type3.getModelist().add(mode5);
        type3.getModelist().add(mode6);
        type3.getModelist().add(mode7);
        type4.getModelist().add(mode8);
        type4.getModelist().add(mode9);
        type5.getModelist().add(mode10);
        type6.getModelist().add(mode11);
        type6.getModelist().add(mode12);
        type7.getModelist().add(mode13);
        type7.getModelist().add(mode14);
        type7.getModelist().add(mode15);
        type7.getModelist().add(mode16);
        type8.getModelist().add(mode17);
        type8.getModelist().add(mode18);
        type8.getModelist().add(mode19);
        type8.getModelist().add(mode20);
        type8.getModelist().add(mode21);
        type8.getModelist().add(mode22);
        type8.getModelist().add(mode23);
        type8.getModelist().add(mode24);
        type9.getModelist().add(mode25);
        type10.getModelist().add(mode26);
        type12.getModelist().add(mode27);
        type12.getModelist().add(mode28);
        type13.getModelist().add(mode29);
        type13.getModelist().add(mode30);
        type14.getModelist().add(mode31);
        type14.getModelist().add(mode32);
        type11.getModelist().add(mode33);


        GetLayerTypes getLayerTypes=new GetLayerTypes();
        getLayerTypes.getType().add(type1);
        getLayerTypes.getType().add(type2);
        getLayerTypes.getType().add(type3);
        getLayerTypes.getType().add(type4);
        getLayerTypes.getType().add(type5);
        getLayerTypes.getType().add(type6);
        getLayerTypes.getType().add(type7);
        getLayerTypes.getType().add(type8);
        getLayerTypes.getType().add(type9);
        getLayerTypes.getType().add(type10);
        getLayerTypes.getType().add(type11);
        getLayerTypes.getType().add(type12);
        getLayerTypes.getType().add(type13);
        getLayerTypes.getType().add(type14);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(getLayerTypes);
        zwsResponse.setRetVal(14);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,GetLayerTypes.class,ZwsType.class,Mode.class,GraphType.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML,stringWriter);


    }

    @Test
    public void LayerIntersectByRadius() throws JAXBException {
        ZwsElement element1 = new ZwsElement();
        element1.setElemID(75729);
        element1.setTypeID(-1);
        element1.setModeNum(-1);

        Records records1 = new Records();
        records1.setBaseID("13");
        records1.setQueryName("Запрос1");

        RecordField recordField1 = new RecordField();
        recordField1.setName("Sys");
        recordField1.setUserName("Sys");
        recordField1.setType(Type.INTEGER);
        recordField1.setKey(KeyLIBR.TRUE);
        recordField1.setReadOnly(ReadOnlyLIBR.TRUE);
        recordField1.setValue("75729");

        RecordField recordField2 = new RecordField();
        recordField2.setName("param1");
        recordField2.setUserName("param1");
        recordField2.setType(Type.STRING);
        recordField2.setKey(null);
        recordField2.setReadOnly(null);
        recordField2.setValue("абвгд");

        RecordField recordField3 = new RecordField();
        recordField3.setName("param2");
        recordField3.setUserName("param2");
        recordField3.setType(Type.FLOAT);
        recordField3.setReadOnly(null);
        recordField3.setKey(null);
        recordField3.setValue("12345");

        RecordField recordField4 = new RecordField();
        recordField4.setName("blob1");
        recordField4.setUserName("blob1");
        recordField4.setType(Type.BLOB);
        recordField4.setReadOnly(null);
        recordField4.setKey(null);
        recordField4.setValue("");
        recordField4.setURL("http://zs.zulugis.ru:6473/zws/GetElemBlob/riga%3Ateplo/75729_Sys:75729/blob1/data.jpg?\n" +
                "                             BaseID=13&QueryName=%D0%97%D0%B0%D0%BF%D1%80%D0%BE%D1%811");

        ZwsElement element2 = new ZwsElement();
        element2.setElemID(75802);
        element2.setTypeID(14);
        element2.setModeNum(1);

        Records records2 = new Records();
        records2.setBaseID(null);


        RecordField recordField5 = new RecordField();
        recordField5.setKey(KeyLIBR.TRUE);
        recordField5.setReadOnly(ReadOnlyLIBR.TRUE);
        recordField5.setName("Sys");
        recordField5.setUserName("Sys");
        recordField5.setType(null);
        recordField5.setValue("75802");

        ZwsElement element3 = new ZwsElement();
        element3.setElemID(75803);
        element3.setTypeID(15);
        element3.setModeNum(1);

        Records records3 = new Records();

        RecordField recordField6 = new RecordField();
        recordField6.setKey(KeyLIBR.TRUE);
        recordField6.setReadOnly(ReadOnlyLIBR.TRUE);
        recordField6.setName("Sys");
        recordField6.setUserName("Sys");
        recordField6.setType(null);
        recordField6.setValue("75803");

        LayerIntersectByRadius layerIntersectByRadius = new LayerIntersectByRadius();
        layerIntersectByRadius.getElements().add(element1);
        layerIntersectByRadius.getElements().add(element2);
        layerIntersectByRadius.getElements().add(element3);

        element1.getRecord().add(records1);
        element2.getRecord().add(records2);
        element3.getRecord().add(records3);

        records1.getRecordField().add(recordField1);
        records1.getRecordField().add(recordField2);
        records1.getRecordField().add(recordField3);
        records1.getRecordField().add(recordField4);
        records2.getRecordField().add(recordField5);
        records3.getRecordField().add(recordField6);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerIntersectByRadius);
        zwsResponse.setRetVal(3);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class, LayerIntersectByRadius.class, ZwsElement.class,Records.class,RecordField.class, ReadOnlyLIBR.class, KeyLIBR.class,Type.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);
    }

    @Test
    public void NetworkAnalyzeSwitch() throws JAXBException {
        ZwsElemen element1 = new ZwsElemen();
        element1.setElemID(19980);
        element1.setState(State.OFF);

        ZwsElemen element2 = new ZwsElemen();
        element2.setElemID(19998);
        element2.setState(State.OFF);

        ZwsElemen element3 = new ZwsElemen();
        element3.setElemID(20000);
        element3.setState(State.OFF);

        ZwsElemen element4 = new ZwsElemen();
        element4.setElemID(20002);
        element4.setState(State.OFF);

        NetworkAnalyzeSwitch networkAnalyzeSwitch = new NetworkAnalyzeSwitch();
        networkAnalyzeSwitch.getElements().add(element1);
        networkAnalyzeSwitch.getElements().add(element2);
        networkAnalyzeSwitch.getElements().add(element3);
        networkAnalyzeSwitch.getElements().add(element4);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(networkAnalyzeSwitch);
        zwsResponse.setRetVal(4);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,NetworkAnalyzeSwitch.class,ZwsElemen.class,State.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(zwsResponse, stringWriter);

        assertEquals(XML, stringWriter);

    }

    @Test
    public void GetLayerBounds() throws JAXBException {
        BoundingBox boundingBox = new BoundingBox();
        boundingBox.setCrs("EPSG:4326");
        boundingBox.setMinx(56.886598);
        boundingBox.setMiny(23.992510);
        boundingBox.setMaxx(56.977003);
        boundingBox.setMaxy(24.116738);

        BoundingBox boundingBox1 = new BoundingBox();
        boundingBox1.setCrs("EPSG:3857");
        boundingBox1.setMinx(7736975.513859);
        boundingBox1.setMiny(2670833.945157);
        boundingBox1.setMaxx(7755419.826169);
        boundingBox1.setMaxy(2684663.016566);


        GetLayerBounds getLayerBounds = new GetLayerBounds();
        getLayerBounds.getBounds().getBoundingBoxes().add(boundingBox);
        getLayerBounds.getBounds().getBoundingBoxes().add(boundingBox1);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(getLayerBounds);
        zwsResponse.setRetVal(0);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,GetLayerBounds.class,ZwsBounds.class,BoundingBox.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);
    } ///////////////////////////////////////////////////////////

    @Test
    public void GetLayerUpdateCount() throws JAXBException {
        GetLayerUpdateCount getLayerUpdateCount = new GetLayerUpdateCount();
        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(getLayerUpdateCount);
        zwsResponse.setRetVal(372);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,GetLayerUpdateCount.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);


    }

    @Test
    public void GetZMMapList() throws JAXBException {
        ZwsMap zwsMap = new ZwsMap();
        zwsMap.setId("1cf221d3-fd7e-4c2d-a2cf-15b39d5868ac");
        zwsMap.setName("Riga");

        ZwsMap zwsMap1 = new ZwsMap();
        zwsMap1.setId("2e2d0599-2d9d-477d-a3d7-3a98f112ca0a");
        zwsMap1.setName("Демо карта");

        ZwsMap zwsMap2 = new ZwsMap();
        zwsMap2.setId("423e0266-88ec-4097-8ea0-8404001f5ee9");
        zwsMap2.setName("Новая карта");

        ZwsMap zwsMap3 = new ZwsMap();
        zwsMap3.setId("7f0dd42b-a7a2-4fd0-b3ca-2cf7f0c65dc7");
        zwsMap3.setName("buve+kreisajs");

        ZwsMap zwsMap4 = new ZwsMap();
        zwsMap4.setId("d01b395f-2efa-4a83-9a71-e907ea2b571f");
        zwsMap4.setName("Новая карта 1");

        ZwsMap zwsMap5 = new ZwsMap();
        zwsMap5.setId("f8dd3c1f-602b-4055-b588-598ecc39763e");
        zwsMap5.setName("Пример карты МО");

        GetZMMapList getZMMapList = new GetZMMapList();
        getZMMapList.getMaps().add(zwsMap);
        getZMMapList.getMaps().add(zwsMap1);
        getZMMapList.getMaps().add(zwsMap2);
        getZMMapList.getMaps().add(zwsMap3);
        getZMMapList.getMaps().add(zwsMap4);
        getZMMapList.getMaps().add(zwsMap5);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(getZMMapList);
        zwsResponse.setRetVal(6);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,GetZMMapList.class,ZwsMap.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);
    }

    @Test
    public void IntersectElemByLayer() throws JAXBException {
        ElementLayer elementLayer = new ElementLayer();
        elementLayer.setElemId(19931);
        elementLayer.setKeys(93859);

        IntersectElemByLayer intersectElemByLayer = new IntersectElemByLayer();
        intersectElemByLayer.getElementLayers().add(elementLayer);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(intersectElemByLayer);
        zwsResponse.setRetVal(1);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,IntersectElemByLayer.class,ElementLayer.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);
    }

    @Test
    public void LayerAddPolygon() throws JAXBException {
        LayerAddPolygon layerAddPolygon = new LayerAddPolygon();
        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerAddPolygon);
        zwsResponse.setRetVal(75803);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerAddPolygon.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);
    }

    @Test
    public void LayerAddPolyline() throws JAXBException {
        LayerAddPolyline layerAddPolyline = new LayerAddPolyline();
        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerAddPolyline);
        zwsResponse.setRetVal(75802);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerAddPolyline.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);
    }

    @Test
    public void LayerAddSymbol() throws JAXBException {
        LayerAddSymbol layerAddSymbol = new LayerAddSymbol();
        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerAddSymbol);
        zwsResponse.setRetVal(75798);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerAddSymbol.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);
    }

    @Test
    public void LayerDeleteElement() throws JAXBException {
        LayerDeleteElement layerDeleteElement = new LayerDeleteElement();
        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerDeleteElement);
        zwsResponse.setRetVal(0);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerDeleteElement.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);
    }

    @Test
    public void LayerDeleteNode() throws JAXBException {
        LayerDeleteNode layerDeleteNode = new LayerDeleteNode();
        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerDeleteNode);
        zwsResponse.setRetVal(0);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerDeleteNode.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);
    }

    @Test
    public void LayerExecSql() throws JAXBException {
        Field field = new  Field();
        field.setName("Sys");
        field.setValue("143");

        Field field1 = new Field();
        field1.setName("Geometry");
        field1.setValue("POINT(56.95237683334503 24.03602916212978)");

        Field field2 = new Field();
        field2.setName("Sys");
        field2.setValue("3378");

        Field field3 = new Field();
        field3.setName("Geometry");
        field3.setValue("LINESTRING(56.96114198632733 24.03543016538373,56.96118286253028\n" +
                "24.03478607840612,56.96093337208149 24.03452454676196,56.96103605433626\n" +
                "24.03419624903532,56.96096206652768 24.03373047962202,56.96044459706884\n" +
                "24.03243668846333)");

        Record record = new Record();
        record.getFields().add(field);
        record.getFields().add(field1);

        Record record1 = new Record();
        record1.getFields().add(field2);
        record1.getFields().add(field3);

        LayerExecSql layerExecSql = new LayerExecSql();
        layerExecSql.getRecords().add(record);
        layerExecSql.getRecords().add(record1);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerExecSql);
        zwsResponse.setRetVal(2);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerExecSql.class,Record.class,Field.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);
    }

    @Test
    public void LayerFindConnected() throws JAXBException {
        LayerFindConnected layerFindConnected =new LayerFindConnected();
        layerFindConnected.setKeys("7 9 10 13 14 15 16 17 18 19 20 21 22 23 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41\n" +
                "              42 43 44 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 63 64 66 67 68 69 70 71 72 73 74 75\n" +
                "              76 77 78 79 80 81 82 83 84 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 104 105\n" +
                "              106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 122 123 124 125 126 127 128\n" +
                "              129 130 131 133 134 135 136 137 138 139 140 141 142 143 145 146 147 149 150 151 152 153\n" +
                "              154 155 156 157 158 159 160 161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 \n" +
                "              176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193 194 196 197 198\n" +
                "              199 200 201 202 203 204 205 206 208 209 210 211 212 213 214 217 219 220 222 223 225 226 \n" +
                "              227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 243 244 245 246 247 248\n" +
                "              249 250 251 252 253 254 255 256 257");

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerFindConnected);
        zwsResponse.setRetVal(13613);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerFindConnected.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);

    }

    @Test
    public void LayerFindWay() throws JAXBException {
        LayerFindWay layerFindWay = new LayerFindWay();
        layerFindWay.setKeys("19919 19922 23785 23784 19921 19924 19923 19926 19925 19936 19935 19938 19937 19940 23781 23780 19939 19946 19945 20005 20004");

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerFindWay);
        zwsResponse.setRetVal(21);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerFindWay.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);
    }

    @Test
    public void LayerGetIncidentElements() throws JAXBException {
        LayerGIElement layerGIElement = new LayerGIElement();
        layerGIElement.setElemID(19939);
        layerGIElement.setKeys("19942 19946 23780 75747");

        LayerGetIncidentElements layerGetIncidentElements = new LayerGetIncidentElements();
        layerGetIncidentElements.getElements().add(layerGIElement);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerGetIncidentElements);
        zwsResponse.setRetVal(4);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerGetIncidentElements.class, LayerGIElement.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);
    }

    @Test
    public void LayerInsertNode() throws JAXBException {
        LayerInsertNode layerInsertNode = new LayerInsertNode();
        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerInsertNode);
        zwsResponse.setRetVal(0);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerInsertNode.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML, stringWriter);

    }

    @Test
    public void LayerIntersectByBox() throws JAXBException {
        //
        LIBElement libElement = new LIBElement();
        libElement.setElemID(75729);
        libElement.setTypeID(-1);
        libElement.setModeNum(-1);

        LIBERecords libeRecords = new LIBERecords();
        libeRecords.setBaseID("13");
        libeRecords.setQueryName("Запрос1");

        LIBEField libeField = new LIBEField();
        libeField.setName("Sys");
        libeField.setUserName("Sys");
        libeField.setType("integer");
        libeField.setKey(KeyLIBB.TRUE);
        libeField.setReadOnly(ReadOnlyLIBB.TRUE);
        libeField.setValue("75729");

        LIBEField libeField1 = new LIBEField();
        libeField1.setName("param1");
        libeField1.setUserName("param1");
        libeField1.setType("string");
        libeField1.setValue("абвгд");
        libeField1.setKey(null);
        libeField1.setReadOnly(null);

        LIBEField libeField2 = new LIBEField();
        libeField2.setName("param2");
        libeField2.setUserName("param2");
        libeField2.setType("float");
        libeField2.setValue("12345");
        libeField2.setKey(null);
        libeField2.setReadOnly(null);

        LIBEField libeField3 = new LIBEField();
        libeField3.setName("blob1");
        libeField3.setUserName("blob1");
        libeField3.getValue();
        libeField3.setType("blob");
        libeField3.getValue();
        libeField3.setUrl("http://zs.zulugis.ru:6473/zws/GetElemBlob/riga%3Ateplo/75729_Sys:75729/blob1/data.jpg?\n" +
                "                             BaseID=13&QueryName=%D0%97%D0%B0%D0%BF%D1%80%D0%BE%D1%811");

        libeField3.setKey(null);
        libeField3.setReadOnly(null);


        //
        LIBElement libElement1 = new LIBElement();
        libElement1.setElemID(75802);
        libElement1.setTypeID(14);
        libElement1.setModeNum(1);

        LIBERecords libeRecords1 = new LIBERecords();

        LIBEField libeField4 = new LIBEField();
        libeField4.setKey(KeyLIBB.TRUE);
        libeField4.setReadOnly(ReadOnlyLIBB.TRUE);
        libeField4.setName("Sys");
        libeField4.setUserName("Sys");
        libeField4.setValue("75802");

        //
        LIBElement libElement2 = new LIBElement();
        libElement2.setElemID(75803);
        libElement2.setTypeID(15);
        libElement2.setModeNum(1);

        LIBERecords libeRecords2 = new LIBERecords();

        LIBEField libeField5 = new LIBEField();
        libeField5.setKey(KeyLIBB.TRUE);
        libeField5.setReadOnly(ReadOnlyLIBB.TRUE);
        libeField5.setName("Sys");
        libeField5.setUserName("Sys");
        libeField5.setValue("75803");

        libElement.getLibeRecords().add(libeRecords);
        libElement1.getLibeRecords().add(libeRecords1);
        libElement2.getLibeRecords().add(libeRecords2);

        libeRecords.getLibeFields().add(libeField);
        libeRecords.getLibeFields().add(libeField1);
        libeRecords.getLibeFields().add(libeField2);
        libeRecords.getLibeFields().add(libeField3);
        libeRecords1.getLibeFields().add(libeField4);
        libeRecords2.getLibeFields().add(libeField5);

        LayerIntersectByBox layerIntersectByBox = new LayerIntersectByBox();
        layerIntersectByBox.getLIBElement().add(libElement);
        layerIntersectByBox.getLIBElement().add(libElement1);
        layerIntersectByBox.getLIBElement().add(libElement2);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerIntersectByBox);
        zwsResponse.setRetVal(3);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerIntersectByBox.class,LIBElement.class,LIBERecords.class,LIBEField.class, ReadOnlyLIBB.class, KeyLIBB.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML,stringWriter);
    }

    @Test
    public void LayerMoveElement() throws JAXBException {
        LayerMoveElement layerMoveElement = new LayerMoveElement();
        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerMoveElement);
        zwsResponse.setRetVal(0);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerMoveElement.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML,stringWriter);

    }

    @Test
    public void LayerMoveNode() throws JAXBException {
        LayerMoveNode layerMoveNode = new LayerMoveNode();
        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerMoveNode);
        zwsResponse.setRetVal(0);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerMoveNode.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML,stringWriter);

    }

    @Test
    public void LayerQueryByExample() throws JAXBException {
        LQRecords lqRecords = new LQRecords();
        lqRecords.setBaseID(2);
        lqRecords.setUserName("Тепловая камера");
        lqRecords.setQueryName("Основной");

        LQField lqField = new LQField();
        lqField.setName("");
        lqField.setUserName("");
        lqField.setKey(layerQueryByExample.Key.TRUE);
        lqField.setReadOnly(layerQueryByExample.ReadOnly.TRUE);
        lqField.setValue("9");

        LQField lqField1 = new LQField();
        lqField1.setName("Nist");
        lqField1.setUserName("Номер источника");
        lqField1.setValue("4");

        LQField lqField2 = new LQField();
        lqField2.setName("Siltumapgades zona");
        lqField2.setUserName("Siltumapgades zona");
        lqField2.setValue("I");

        LQField lqField3 = new LQField();
        lqField3.setName("Blob1");
        lqField3.setUserName("Blob1");
        lqField3.getValue();
        lqField3.getUrl();

        LQField lqField4 = new LQField();
        lqField4.setName("DateTime1");
        lqField4.setUserName("DateTime1");
        lqField4.getValue();

        LQField lqField5 = new LQField();
        lqField5.setName("Date1");
        lqField5.setUserName("Date1");
        lqField5.getValue();

        LQField lqField6 = new LQField();
        lqField6.setName("Time1");
        lqField6.setUserName("Time1");
        lqField6.getValue();

        LQField lqField7 = new LQField();
        lqField7.setName("TR");
        lqField7.setUserName("TR");
        lqField7.setValue("2");

        LQField lqField8 = new LQField();
        lqField8.setName("Kat");
        lqField8.setUserName("Kategorija");
        lqField8.setValue("M");

        LQField lqField9 = new LQField();
        lqField9.setName("Pied");
        lqField9.setUserName("Piederiba");
        lqField9.setValue("RS");

        LQField lqField10 = new LQField();
        lqField10.setName("Pied");
        lqField10.setUserName("Piederiba");
        lqField10.setValue("RS");

        LQField lqField11 = new LQField();
        lqField11.setName("Pied");
        lqField11.setUserName("Piederiba");
        lqField11.setValue("RS");

        LQField lqField12 = new LQField();
        lqField12.setName("Pied");
        lqField12.setUserName("Piederiba");
        lqField12.setValue("RS");

        LQField lqField13 = new LQField();
        lqField13.setName("Pied");
        lqField13.setUserName("Piederiba");
        lqField13.setValue("RS");

        LQField lqField14 = new LQField();
        lqField14.setName("Shema");
        lqField14.setUserName("Shema");
        lqField14.setUrl("http://zs.zulugis.ru:6473/zws/GetElemBlob/riga%3Ateplo/9_Sys:9/Shema/data.jpg?\n" +
                "                         BaseID=2&amp;QueryName=%D0%9E%D1%81%D0%BD%D0%BE%D0%B2%D0%BD%D0%BE%D0%B9");


        LQField lqField15 = new LQField();
        lqField15.setName("H_geo");
        lqField15.setUserName("Геодезическая отметка, м");
        lqField15.setValue("9");

        LQField lqField16 = new LQField();
        lqField16.setName("K_ism");
        lqField16.setUserName("K_ism");
        lqField16.setValue("D");

        LQField lqField17 = new LQField();
        lqField17.setName("Jeg_dzil");
        lqField17.setUserName("Jeguldisanas dzilums");
        lqField17.getValue();

        LQField lqField18 = new LQField();
        lqField18.setName("Izb_v");
        lqField18.setUserName("Izbuves veids");
        lqField18.getValue();

        LQField lqField19 = new LQField();
        lqField19.setName("Izb_g");
        lqField19.setUserName("Izbuves gads");
        lqField19.getValue();

        LQField lqField20 = new LQField();
        lqField20.setName("Sig");
        lqField20.setUserName("Signalizacija");
        lqField20.getValue();

        LQField lqField21 = new LQField();
        lqField21.setName("Sig_sta");
        lqField21.setUserName("Signalizacija stavoklis");
        lqField21.getValue();

        LQField lqField22 = new LQField();
        lqField22.setName("H_pod");
        lqField22.setUserName("Напор в подающем трубопроводе, м");
        lqField22.setValue("83.997");

        LQField lqField23 = new LQField();
        lqField23.setName("H_obr");
        lqField23.setUserName("Напоp в обpатном тpубопpоводе, м");
        lqField23.setValue("29.003");

        LQField lqField24 = new LQField();
        lqField24.setName("Tb");
        lqField24.setUserName("Давление вскипания, м");
        lqField24.setValue("-6.34");

        LQField lqField25 = new LQField();
        lqField25.setName("Hstat");
        lqField25.setUserName("Статический напор, м");
        lqField25.setValue("31");

        LQField lqField26 = new LQField();
        lqField26.setName("Hstat_out");
        lqField26.setUserName("Статический напор на выходе, м");
        lqField26.setValue("31");

        LQField lqField27 = new LQField();
        lqField27.setName("Gpod");
        lqField27.setUserName("Слив из подающего трубопровода, т/ч");
        lqField27.getValue();

        LQField lqField28 = new LQField();
        lqField28.setName("Gobr");
        lqField28.setUserName("Слив из обратного трубопровода, т/ч");
        lqField28.getValue();

        LayerQueryByExample layerQueryByExample = new LayerQueryByExample();
        layerQueryByExample.getLqRecords().add(lqRecords);
        layerQueryByExample.setRecordCount(1);

        lqRecords.getLqFields().add(lqField);
        lqRecords.getLqFields().add(lqField1);
        lqRecords.getLqFields().add(lqField2);
        lqRecords.getLqFields().add(lqField3);
        lqRecords.getLqFields().add(lqField4);
        lqRecords.getLqFields().add(lqField5);
        lqRecords.getLqFields().add(lqField6);
        lqRecords.getLqFields().add(lqField7);
        lqRecords.getLqFields().add(lqField8);
        lqRecords.getLqFields().add(lqField9);
        lqRecords.getLqFields().add(lqField10);
        lqRecords.getLqFields().add(lqField11);
        lqRecords.getLqFields().add(lqField12);
        lqRecords.getLqFields().add(lqField13);
        lqRecords.getLqFields().add(lqField14);
        lqRecords.getLqFields().add(lqField15);
        lqRecords.getLqFields().add(lqField16);
        lqRecords.getLqFields().add(lqField17);
        lqRecords.getLqFields().add(lqField18);
        lqRecords.getLqFields().add(lqField19);
        lqRecords.getLqFields().add(lqField20);
        lqRecords.getLqFields().add(lqField21);
        lqRecords.getLqFields().add(lqField22);
        lqRecords.getLqFields().add(lqField23);
        lqRecords.getLqFields().add(lqField24);
        lqRecords.getLqFields().add(lqField25);
        lqRecords.getLqFields().add(lqField26);
        lqRecords.getLqFields().add(lqField27);
        lqRecords.getLqFields().add(lqField28);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(layerQueryByExample);
        zwsResponse.setRetVal(0);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,LayerQueryByExample.class,LQRecords.class,LQField.class, layerQueryByExample.Key.class, layerQueryByExample.ReadOnly.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML,stringWriter);

    }

    @Test
    public void LayerReadCustomData() throws JAXBException {
        LayerReadCustomData layerReadCustomData = new LayerReadCustomData();
        layerReadCustomData.setName("_zt.tl");
        layerReadCustomData.setLayer("riga:teplo");

        Command command = new Command();
        command.setLayerReadCustomData(layerReadCustomData);

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(command);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,Command.class, LayerReadCustomData.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML,stringWriter);
    }

    @Test
    public void NetworkRecalc() throws JAXBException {
        NetworkRecalc networkRecalc = new NetworkRecalc();
        networkRecalc.setCount(55);
        networkRecalc.setKeys("14383 14404 19588 10261 10273 13543 10285 10265 13547 19590 19153 10242 10253 19615\n" +
                "            10248 10257 10206 10212 10218 10226 17933 9220 9224 14770 5614 7007 5774 5758 5764 5770\n" +
                "            5780 5784 7001 5309 10287 10295 10303 10311 10335 10343 10351 10317 10333 19617 19611\n" +
                "            10281 10321 10329 10291 10307 10339 10347 10361 19661 10277");

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(networkRecalc);
        zwsResponse.setRetVal(55);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,NetworkRecalc.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML,stringWriter);
    }

    @Test
    public void UpdateElemAttributes() throws JAXBException {
        UpdateElemAttributes updateElemAttributes = new UpdateElemAttributes();

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(updateElemAttributes);
        zwsResponse.setRetVal(0);


        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,UpdateElemAttributes.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML,stringWriter);

    }

    @Test
    public void SetElemState() throws JAXBException {
        SetElemState setElemState = new SetElemState();

        ZwsResponse zwsResponse = new ZwsResponse();
        zwsResponse.setResponse(setElemState);
        zwsResponse.setRetVal(0);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,SetElemState.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML,stringWriter);
    }

    @Test
    public void TrackingGetLayerList() throws JAXBException {
        Layer layer = new Layer();
        layer.setName("tracking");
        layer.setTitle("TrackingObjects1");

        Layer layer1 = new Layer();
        layer1.setName("ZSTracking");
        layer1.setTitle("ZSTracking");

        TrackingGetLayerList trackingGetLayerList = new TrackingGetLayerList();
        trackingGetLayerList.getLayers().add(layer);
        trackingGetLayerList.getLayers().add(layer1);

        ZwsResponse zwsResponse= new ZwsResponse();
        zwsResponse.setResponse(trackingGetLayerList);
        zwsResponse.setRetVal(2);

        StringWriter stringWriter = new StringWriter();
        JAXBContext context = JAXBContext.newInstance(ZwsResponse.class,TrackingGetLayerList.class,Layer.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,Boolean.TRUE);
        marshaller.marshal(zwsResponse,stringWriter);

        assertEquals(XML,stringWriter);
    }
}

